const jwt = require('jsonwebtoken');
const authenToken = (req, res, next) => {
    let token = '';
    console.log(' vào authenToken....')
    let authorizetionHeader = req.headers.authorization || req.headers.cookie
    let token0 = authorizetionHeader ? authorizetionHeader.split('token=')[1] : null;
    console.log('token0', token0);
    var authorizetionHeader1 = req.headers.cookie;
    let token1 = authorizetionHeader1 ? authorizetionHeader1.split('=')[1] : null;
    console.log("token1", token1)
    token = token0 ? token0 : token1;
    if (!token || token == "Bearer" || token == null) {
        return res.redirect('/loginshop');
    }
    console.log('token', token);
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, data) => {
        if (err) {
            err = {
                name: 'JsonWebTokenError',
                message: 'jwt signature is required',
            }
            console.log("err", err)
            return res.redirect('/loginshop');
        }
        req.infoUser = data.data;
        req.idUser;
        req.token = token;
        next();
    });
}
module.exports = {
    authenToken,
};