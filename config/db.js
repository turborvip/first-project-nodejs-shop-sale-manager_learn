const mysql = require('mysql2/promise');
const { Sequelize } = require('sequelize');
const bcrypt = require('bcryptjs');
require('dotenv').config();

const { host, port, user, password, database } = {
    "host": process.env.DATABASE_HOST,
    "port": process.env.DATABASE_PORT,
    "user": process.env.DATABASE_USER,
    "password": process.env.DATABASE_PASSWORD,
    "database": process.env.DATABASE_NAME,
};

module.exports = db = {};
initialize();
async function initialize() {
    const connection = await mysql.createConnection({ host, port, user, password });
    await connection.query(`CREATE DATABASE IF NOT EXISTS \`${database}\`;`);
    const sequelize = new Sequelize(database, user, password,
        {
            host: host,
            dialect: 'mysql',
            query: { raw: true },
            logQueryParameters: true,
            // timezone: process.env.TimeZone,
            dialectOptions: {
                useUtc: false,
            }
        }
    );
    db.User = require('../models/user.model').model(sequelize);
    db.Category = require('../models/category.model').model(sequelize);
    db.Product = require('../models/product.model').model(sequelize);
    db.Order = require('../models/order.model').model(sequelize);
    db.Order_product = require('../models/order_product.model').model(sequelize);
    db.News = require('../models/news.model').model(sequelize);
    db.Discount = require('../models/discount.model').model(sequelize);
    db.Discount_product = require('../models/discount_product.model').model(sequelize);
    db.sequelize = sequelize;
    await sequelize.sync();
}

