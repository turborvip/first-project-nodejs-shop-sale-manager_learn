-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: shop_nodejs
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_name` text NOT NULL,
  `password` text NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `phone` int DEFAULT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  `address` varchar(255) DEFAULT '',
  `image` varchar(255) DEFAULT NULL,
  `role` tinyint NOT NULL,
  `status` tinyint NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `del` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'turborvip','$2a$10$diXnOzMs3xXNTA3OlchFoOynYmwfGcCRFiOhgqCZbb643g1S9M7gG','Đỗ Thành Đạt','turborvip@gmail.com',387661333,'Boss','Ha Noi','9-500x500-1646046694314.jpg',3,1,'2021-11-18 07:17:32','2022-02-28 11:11:36',0),(2,'turborvip1','$2a$10$Xuc7ItHUm22kgpQi09FoguS8OQ8Pnqd5M.Erw8QMA/iHsty45y4ka','Đỗ Đức Luân','aaaaaa@gmail.com',NULL,'aaaaa','hà nam','Day-la-dau-toi-la-ai-chuyen-gi-xay-ra-the.jpg',1,0,'2021-11-18 07:31:32',NULL,0),(4,'turborvip2','$2a$10$3sy5RuPkyUwrJbXuj9hvq.NY9HCZ7iRp8J66bAr5LHg/mFxZPh052','Đỗ Thành Đạt','turborvip@gmail.com',387661380,'hihi','Ha Noi','197706318_237454474855553_6843993550435929307_n.jpg',2,0,'2021-11-26 02:59:49','2021-11-26 03:42:21',0),(5,'sadfsd','$2a$10$wcwnYui4GAD3ZDOkmpFucuJoeMKnNrg8TMuuiXXFUlDsMz6.t96au','sdfsdff','turbosssrvip@gmail.com',948668875,'sdfsdfsdfsdf','sdfsdf','ghẹ-1639557401574.jpg',1,0,'2021-12-15 08:37:07',NULL,0),(6,'turborvip4','$2a$10$t4jC6Yh.dUHUrlR/Zu7r8.SDTirsnKFBY6S/tclccmHcLBK27v5Jy','Đỗ Thành Đạt','turborvip@gmail.com',387661380,'','',NULL,1,1,'2021-12-23 08:22:01','2021-12-24 03:36:03',0),(7,'chieuhomay','$2a$10$jCMwwc7HKTSn.tyDu0jqZOiwfnUL8XststajFtkCwB4EZUloJqDpK','','',NULL,'','',NULL,1,1,'2022-04-12 14:18:49',NULL,0),(8,'turborvip123','$2a$10$aocs1AuDR3cFf4YEdxv.W.ZuqvEM9abft0aFW1fg3XOZ8cCxElkS.','','',NULL,'','',NULL,1,1,'2022-04-21 08:16:38',NULL,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-15  7:34:23
