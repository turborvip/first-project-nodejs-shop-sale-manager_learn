-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: shop_nodejs
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_category` int NOT NULL,
  `name` text NOT NULL,
  `price` varchar(255) NOT NULL DEFAULT '',
  `unit` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) DEFAULT '',
  `size` varchar(255) DEFAULT '',
  `color` varchar(255) DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `del` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,20,'Air jordan 1 high satin black toe','1000','USD','[\"air-jordan-1-high-satin-black-toe-waa-600x600-1639705526884.jpg\",\"air-jordan-1-high-satin-black-toe-wa-600x600-1639705526879.jpg\",\"air-jordan-1-high-satin-black-toe-w-600x600-1639705526870.png\"]','[\"5.5US\",\"6US\",\"6.5US\",\"7US\",\"7.5US\",\"8US\",\"8.5US\",\"9US\"]','','giày jordan high',1,'2021-12-17 01:45:29',NULL,0),(2,19,'Jordan low black sliver','200','USD','[\"air-jordan-1-low-black-silver-da5551-0013-768x768-1639705594785.jpg\",\"air-jordan-1-low-black-silver-da5551-0012-768x768-1639705594780.jpg\",\"air-jordan-1-low-black-silver-da5551-0011-768x768-1639705594776.jpg\"]','[\"6.5US\",\"7US\",\"7.5US\",\"8.5US\"]','','giày jordan cổ thấp đen xám',1,'2021-12-17 01:46:37',NULL,0),(3,19,'Air jordan 1 low green toe','500','USD','[\"air-jordan-1-low-green-toe-553558-371-6-600x600-1639705812159.jpg\",\"air-jordan-1-low-green-toe-553558-371-1-600x600-1639705812153.jpg\",\"air-jordan-1-low-green-toe-1-553558-371-600x600-1639705812151.jpg\"]','[\"7US\",\"7.5US\"]','','giày jordan 1 low',1,'2021-12-17 01:50:13',NULL,0),(4,19,'Air jordan 1 low siren red ','560','USD','[\"air-jordan-1-low-siren-red-DC0774-004-2-768x768-1639705895798.jpg\",\"air-jordan-1-low-siren-red-DC0774-004-1-768x768-1639705895795.jpg\",\"air-jordan-1-low-siren-red-1-DC0774-004-768x768-1639705895792.jpg\"]','[\"6.5US\",\"7US\",\"7.5US\",\"8US\"]','','giày jordan hồng',1,'2021-12-17 01:51:37',NULL,0),(5,19,'Air jordan 1 low starfish','360','USD','[\"air-jordan-1-low-starfish-cz0790-801-02-600x600-1639706042361.jpg\",\"air-jordan-1-low-starfish-cz0790-801-01-600x600-1639706042355.jpg\",\"air-jordan-1-low-starfish-a-cz0790-801-600x600-1639706042351.jpg\"]','[\"5.5US\",\"7US\",\"7.5US\",\"8US\"]','','giày jd low cam',1,'2021-12-17 01:54:09',NULL,0),(6,19,'Air jordan 1 low unc  ','260','USD','[\"air-jordan-1-low-unc-bAO9944-441-12-600x600-1639706150640.jpg\",\"air-jordan-1-low-unc-AO9944-441-1-600x600-1639706150635.jpg\",\"air-jordan-1-low-unc-a-AO9944-441-600x600-1639706150629.jpg\"]','[\"6.5US\",\"7US\",\"8US\",\"8.5US\"]','','jordan low 1 xanh dương',1,'2021-12-17 01:55:52',NULL,0),(7,20,'Air jordan 1 mid black tone ','450','USD','[\"air-jordan-1-mid-black-white-BQ6472-011-2-600x600-1639706249718.jpg\",\"air-jordan-1-mid-black-white-BQ6472-011-1-600x600-1639706249713.jpg\",\"air-jordan-1-mid-black-white-a-BQ6472-011-600x600-1639706249710.jpg\"]','[\"7US\",\"7.5US\"]','','jordan 1 high đen',1,'2021-12-17 01:57:32',NULL,0),(8,20,'Air jordan 1 mid electro Orange','860','USD','[\"air-jordan-1-mid-electro-orange-dm3531-800-2-600x600-1639706368110.jpg\",\"air-jordan-1-mid-electro-orange-dm3531-800-1-600x600-1639706367785.jpg\",\"air-jordan-1-mid-electro-orange-a-dm3531-800-600x600-1639706367781.jpg\"]','[\"6.5US\",\"7US\",\"7.5US\",\"8US\",\"8.5US\",\"9US\"]','','jordan 1 orange',1,'2021-12-17 01:59:30',NULL,0),(9,20,'Air jordan 1 mid metallic gold black','980','USD','[\"air-jordan-1-mid-metallic-gold-black-DC1419-700-2-600x600-1639706458370.jpg\",\"air-jordan-1-mid-metallic-gold-black-DC1419-700-1-600x600-1639706458363.jpg\",\"air-jordan-1-mid-metallic-gold-black-a-DC1419-700-600x600-1639706458356.jpg\"]','[\"7US\",\"7.5US\",\"8US\"]','','jordan 1 medilac',1,'2021-12-17 02:00:59',NULL,0),(10,20,'Air jordan 1 high obisan ','1090','USD','[\"air-jordan-1-retro-high-og-obsidian-unc-575441-140-2-768x768-1639706837113.jpg\",\"air-jordan-1-retro-high-og-b-obsidian-unc-575441-1400-768x768-1639706837109.jpg\",\"air-jordan-1-retro-high-og-a-obsidian-unc-a-575441-140-768x768-1639706837103.jpg\"]','[\"7US\",\"7.5US\",\"8US\"]','','i\'ll be shot if i know',1,'2021-12-17 02:07:19',NULL,0),(11,20,'Air jordan 3 retro red cement','3640','USD','[\"air-jordan-3-retro-se-fire-red-CQ0488-600-3-768x768-1639706940914.jpg\",\"air-jordan-3-retro-se-fire-red-CQ0488-600-2-768x768-1639706940907.jpg\",\"air-jordan-3-retro-se-fire-red-CQ0488-600-1-768x768-1639706940899.jpg\"]','[\"7.5US\"]','','jordan 3 ',1,'2021-12-17 02:09:04',NULL,0),(12,5,'Drew house mascot original','80','USD','[\"ao-drew-house-mascot-sky-blue-d-1639707795930.jpg\",\"ao-drew-house-mascot-off-white-c-1639707795923.jpg\",\"ao-drew-house-mascot-lavender-a-1639707795913.jpg\",\"ao-drew-house-mascot-a-off-a-white-a-1639707795900.jpg\"]','[\"S\",\"M\",\"L\",\"XL\"]','[\"#e5e5d3\",\"#bba3d3\",\"#4c8ad7\"]','Drew house mascot original',1,'2021-12-17 02:23:17',NULL,0),(13,5,'Drew house hearty vintage black','90','USD','[\"ao-drew-house-hearty-vintage-black-dh-sj2121-htbk-3-600x600-1639707885344.jpg\",\"ao-drew-house-hearty-vintage-black-dh-sj2121-htbk-2-600x600-1639707885339.jpg\",\"ao-drew-house-hearty-vintage-black-a-1639707885334.jpg\"]','[\"M\",\"L\"]','','drew house',1,'2021-12-17 02:24:47',NULL,0),(14,5,'Drew house brown tye','120','USD','[\"ao-drew-house-brown-tie-dye-dh-hj2121-mcbrtd-3-600x600-1639707965169.jpg\",\"ao-drew-house-brown-tie-dye-dha-1639707965162.jpg\"]','[\"S\",\"M\",\"L\",\"XL\"]','','drew house',1,'2021-12-17 02:26:06',NULL,0),(15,24,'Gucci Glass','2500','USD','[\"gucci-gg0566o-004-1-600x600-1-1639708030711.jpg\",\"gguc-g025146-m108510-bi-12-1639708030704.jpg\",\"gguc-g025146-m108510-bi-1-1639708030702.jpg\"]','','','Gucci Glass',1,'2021-12-17 02:27:12',NULL,0),(16,24,'Gentle monster Glass','1500','USD','[\"kinh-gentle-monster-unaa-c-01-600x600-1639708107981.jpg\",\"kinh-gentle-monster-b-1639708107978.jpg\",\"kinh-gentle-monster-a-1639708107972.jpg\"]','','','Gentle monster Glass',1,'2021-12-17 02:28:29',NULL,0),(17,25,'MLB monogram rainbow hoody multicolor white','5000','USD','[\"mlb-monogram-rainbow-y-hoody-multicolor-white-b-1639709087974.jpg\",\"mlb-monogram-rainbow-hoody-multicolor-white-c-1639709087962.jpg\",\"mlb-monogram-rainbow-hoody-multicolor-white-a-1639709087955.jpg\"]','','','mini bag',1,'2021-12-17 02:30:01','2021-12-17 02:44:52',0),(18,25,'MLB cross bag monogram jacquard navy mini ','3500','USD','[\"tui-mlb-cross-bag-monogram-y-1639709147774.jpg\",\"tui-mlb-cross-bag-monogram-c-1639709147772.jpg\",\"tui-mlb-cross-bag-monogram-a-1639709147766.jpg\"]','','','MLB',1,'2021-12-17 02:45:49',NULL,0),(19,25,'MLB mini bag original','1500','USD','[\"tui-mlb-hobo-bag-solid-ny-black-3abqs051n-50bks-600x600-1639709203594.jpg\",\"tui-mlb-hobo-bag-solid-ny-black-3abqs051n-50bks-4-600x600-1639709203562.jpg\",\"tui-mlb-hobo-bag-solid-ny-black-3abqs051n-50bks-1-600x600-1639709203554.jpg\"]','','','all_image_product',1,'2021-12-17 02:46:44',NULL,0),(20,22,'Beanie jordan','50','USD','[\"mu-beanie-jordan-grey-9a0588-geh-600x600-1639709256207.jpg\",\"mu-beanie-jordan-grey-9a0588-geh-1-600x600-1639709256200.jpg\"]','','','all_image_product',1,'2021-12-17 02:47:38',NULL,0),(21,23,'MLB rookie ball la brown Cap','70','USD','[\"mu-mlb-rookie-ball-la-brown-d-1639709320135.jpg\",\"mu-mlb-rookie-ball-la-brown-c-1639709320129.jpg\",\"mu-mlb-rookie-ball-la-brown-a-1639709320126.jpg\"]','','','all_image_product',1,'2021-12-17 02:48:42',NULL,0),(22,26,'Fila linear logo long crew white socks','5','USD','[\"tat-vo-fila-linear-logo-long-crew-white-d-1639709429286.jpg\",\"tat-vo-fila-linear-logo-long-crew-white-b-1639709429277.jpg\",\"tat-vo-fila-linear-logo-long-crew-white-a-1639709429260.jpg\"]','','','all_image_product',1,'2021-12-17 02:50:31',NULL,0);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-15  7:34:23
