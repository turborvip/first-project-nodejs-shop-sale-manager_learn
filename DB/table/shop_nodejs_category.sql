-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: shop_nodejs
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `parent_id` int NOT NULL,
  `title` text NOT NULL,
  `description` varchar(255) NOT NULL,
  `gender` tinyint NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `del` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,0,'TOPS','tất cả các loại áo',0,'tops-1639667542924.jpg',1,'2021-12-16 15:12:24',NULL,0),(2,0,'BOTTOMS','tất cả các loại quần',0,'bottoms-1639667578793.jpg',1,'2021-12-16 15:13:00',NULL,0),(3,0,'SHOES','tất cả các loại giày',0,'shoes-1639667609726.jpg',1,'2021-12-16 15:13:32',NULL,0),(4,0,'ACCESSORY','tất cả các phụ kiện',0,'accessory-1639667638233.jpg',1,'2021-12-16 15:14:01',NULL,0),(5,1,'T-shirt','áo ngắn tay các loại',0,'t-shirt-1639667819001.jpg',1,'2021-12-16 15:17:00',NULL,0),(6,1,'Shirt','áo sơ mi nam ',1,'shirt-1639667845423.jpg',1,'2021-12-16 15:17:27',NULL,0),(7,1,'Polo shirt','áo polo nam',1,'polo-shirt-1639667877029.jpg',1,'2021-12-16 15:17:58',NULL,0),(8,1,'Hoodie','áo hoodie ',0,'hoodie-1639667900671.jpg',1,'2021-12-16 15:18:22',NULL,0),(9,1,'Shirts and blouses','áo sơ mi nữ',2,'shirts-and-blouses-1639667936075.jpg',1,'2021-12-16 15:18:58',NULL,0),(10,1,'Cardigans','áo cardigans nữ',2,'cardigans-1639667966311.png',1,'2021-12-16 15:19:28',NULL,0),(11,1,'Bratop','áo 2 dây nữ',2,'bratop-1639667994176.jpg',1,'2021-12-16 15:19:56',NULL,0),(12,2,'Short','quần đùi',0,'short-1639668023978.jpg',1,'2021-12-16 15:20:25',NULL,0),(13,2,'Trousers','quần âu nam',2,'trousers-1639668052969.jpeg',1,'2021-12-16 15:20:55',NULL,0),(14,2,'Ankle Pants','quần ống xuông nam',1,'ankle pants-1639668087094.jpg',1,'2021-12-16 15:21:28',NULL,0),(15,2,'Jeans','quần jeans',0,'jeans-1639668109385.jpg',1,'2021-12-16 15:21:51',NULL,0),(16,2,'Skirts and Dresses','các loại váy nữ',2,'skirts-1639668142096.jpg',1,'2021-12-16 15:22:23',NULL,0),(17,2,'Leggings Pants','quần bó nữ',2,'leggings pants-1639668163766.jpg',1,'2021-12-16 15:22:46',NULL,0),(18,3,'Jordan','giày các loại',0,'jordan shoes-1639668205498.jpg',1,'2021-12-16 15:23:28',NULL,0),(19,18,'Low','giày cổ thấp',0,'jordan low-1639668536744.jpg',1,'2021-12-16 15:24:04','2021-12-16 15:28:58',0),(20,18,'High','giày cổ cao',0,'jordan shoes-1639668280574.jpg',1,'2021-12-16 15:24:42',NULL,0),(21,3,'Gucci','giày gucci',0,'gucci shoes-1639668302953.jpg',1,'2021-12-16 15:25:04',NULL,0),(22,4,'Beanie','mũ len',0,'beanie-1639668322966.jpg',1,'2021-12-16 15:25:24',NULL,0),(23,4,'Cap','mũ lưỡi trai',0,'cap-1639668343670.jpg',1,'2021-12-16 15:25:45',NULL,0),(24,4,'Glass','kính',0,'glass-1639668361985.jpg',1,'2021-12-16 15:26:03',NULL,0),(25,4,'Mini bag','túi sách',0,'mini bag-1639668383311.jpg',1,'2021-12-16 15:26:25',NULL,0),(26,4,'Socks','tất',0,'socks-1639668403566.jpg',1,'2021-12-16 15:26:44',NULL,0),(27,0,'ádasdas','ádasd',0,'mini bag-1639668974123.jpg',0,'2021-12-16 15:36:19',NULL,0);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-15  7:34:22
