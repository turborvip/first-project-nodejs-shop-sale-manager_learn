const express = require('express');
const router = express.Router();
const auth = require("./middleware/auth");

const loginShop = require('./controller/admin/loginshop.controller');
const dashboard = require('./controller/admin/dashboard.controller');
const category = require('./controller/admin/category.controller');
const product = require('./controller/admin/product.controller');
const news = require('./controller/admin/news.controller');
const discount = require('./controller/admin/discount.controller');
const users = require('./controller/admin/users.controller');
const order = require('./controller/admin/order.controller');

const loginController = require('./controller/client/login.controller');
const client = require('./controller/client/client.controller');
const homePage = require('./controller/client/home.controller');
const detailPage = require('./controller/client/detail.controller');
const categoryPage = require('./controller/client/category.controller');
const productPage = require('./controller/client/product.controller');
const newsPage = require('./controller/client/news.controller');
const paymentPage = require('./controller/client/payment.controller');
const account = require('./controller/client/account.controller');

////////////////// Admin

//Login
router.get("/loginshop", loginShop.login);
router.post("/loginshop/check", loginShop.loginCheck);
router.get("/logoutshop", loginShop.logOut);

//Dashboard
router.get("/dashboard", auth.authenToken, dashboard.dashboard);

//news
router.get("/dashboard/listnews/update/:id", auth.authenToken, news.updateNews);
router.post("/dashboard/listnews/update", auth.authenToken, news.updateNewsPost);
router.get("/dashboard/listnews", auth.authenToken, news.listNews);
router.post("/dashboard/listnews/delete", auth.authenToken, news.deleteNews);
router.post("/dashboard/listnews/active", auth.authenToken, news.activeNews);
router.get("/dashboard/createnews", auth.authenToken, news.createNews);
router.post("/dashboard/createnews", auth.authenToken, news.createNewsPost);

//discount
router.post("/dashboard/list-discount-product/delete", auth.authenToken, discount.deleteDiscountProduct);
router.get("/dashboard/list-discount-product", auth.authenToken, discount.listDiscountProduct);
router.post("/dashboard/list-discount-product", auth.authenToken, discount.listDiscountProduct);
router.get("/dashboard/add-discount-product/:id", auth.authenToken, discount.addDiscountProduct);
router.post("/dashboard/add-discount-product", auth.authenToken, discount.addDiscountProductPost);
router.get("/dashboard/listdiscount/update/:id", auth.authenToken, discount.updateDiscount);
router.post("/dashboard/listdiscount/update", auth.authenToken, discount.updateDiscountPost);
router.get("/dashboard/listdiscount", auth.authenToken, discount.listDiscount);
router.post("/dashboard/listdiscount/delete", auth.authenToken, discount.deleteDiscount);
router.post("/dashboard/listdiscount/active", auth.authenToken, discount.activeDiscount);
router.get("/dashboard/creatediscount", auth.authenToken, discount.createDiscount);
router.post("/dashboard/creatediscount", auth.authenToken, discount.createDiscountPost);

//order
router.get("/dashboard/listorder", auth.authenToken, order.listOrder);
router.get("/dashboard/listorder/detail", auth.authenToken, order.detailOrder);
router.post("/dashboard/listorder/delete", auth.authenToken, order.deleteOrder);
router.post("/dashboard/listorder/status", auth.authenToken, order.statusOrder);


//category
router.get("/dashboard/listcategory/update/:id", auth.authenToken, category.updateCategory);
router.post("/dashboard/listcategory/update", auth.authenToken, category.updateCategoryPost);
router.get("/dashboard/listcategory", auth.authenToken, category.listCategory);
router.post("/dashboard/listcategory/delete", auth.authenToken, category.deleteCategory);
router.post("/dashboard/listcategory/active", auth.authenToken, category.activeCategory);
router.get("/dashboard/createcategory", auth.authenToken, category.createCategory);
router.post("/dashboard/createcategory", auth.authenToken, category.createCategoryPost);

// Product
router.get("/dashboard/listproduct/update/:id", auth.authenToken, product.updateProduct);
router.post("/dashboard/listproduct/update", auth.authenToken, product.updateProductPost);
router.get("/dashboard/listproduct", auth.authenToken, product.listProduct);
router.post("/dashboard/listproduct/delete", auth.authenToken, product.deleteProduct);
router.post("/dashboard/listproduct/active", auth.authenToken, product.activeProduct);
router.get("/dashboard/createproduct", auth.authenToken, product.createProduct);
router.post("/dashboard/createproduct", auth.authenToken, product.createProductPost);


//users
router.post("/dashboard/update-user", auth.authenToken, users.updateUsers);
router.post("/dashboard/changepass", auth.authenToken, users.changePass);
router.get("/dashboard/listusers", auth.authenToken, users.listUsers);
router.post("/dashboard/listusers/delete", auth.authenToken, users.deleteUsers);
router.post("/dashboard/listusers/active", auth.authenToken, users.activeUsers);
router.get("/dashboard/createusers", auth.authenToken, users.createUsers);
router.post("/dashboard/createusers", auth.authenToken, users.createUsersPost);


///////////////////Client
//Login
router.get("/login", loginController.login); // cái middleware là cái phần ở giữa phương thức http đến action  mình mình sẽ viết nó ởchooa này
router.post('/register', loginController.register);
router.post('/login/check', loginController.loginCheck);

//Menu
router.post("/menu", client.menu);

//Home
router.get("/home", homePage.home);

//Detail
router.get("/detail", detailPage.deatailProduct);

//Category
router.get("/category", categoryPage.category);

//Product
router.get("/product", productPage.product);

//News
router.get("/news", newsPage.news);

//Payment
router.get("/payment", paymentPage.payment);
router.post("/payment/district", paymentPage.getDistrict);
router.post("/payment/ward", paymentPage.getWard);
router.post("/createOrder", order.createOrder);

//Account
router.get("/account", account.account);
router.post("/account/updateInf", account.updateInf);
router.post("/account/changepass", account.changepass);
router.post("/account/history", account.history);

///////////////////Client2
router.get("/turborvipshop/menu", client.menu);
router.get("/turborvipshop/home", homePage.home);


module.exports = router