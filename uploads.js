const express = require('express');
const uploads = express.Router();
const upload = require('./controller/upload.controller');
const path = require('path');
const multer = require('multer');
const { count } = require('console');

const storage_category = multer.diskStorage({
    destination: function (req, file, res) {
        res(null, './static/image/category');
    },
    filename: function (req, file, cb) {
        console.log('vao fike name', file.fieldname);
        console.log('body', req.body);
        let name_file = file.originalname.split(".")[0] + '-' + Date.now() + path.extname(file.originalname)
        req.body.file = name_file;
        cb(null, name_file);
    }
});

const storage_product = multer.diskStorage({
    destination: function (req, file, res) {
        res(null, './static/image/product');
    },
    filename: function (req, file, cb) {
        console.log('vao fike name');
        console.log('vao fike name', file.fieldname);
        console.log('body', req.body);
        let name_file = file.originalname.split(".")[0] + '-' + Date.now() + path.extname(file.originalname);
        req.body.file = [name_file].concat(req.body.file);
        cb(null, name_file);
    }
});

const storage_news = multer.diskStorage({
    destination: function (req, file, res) {
        res(null, './static/image/news');
    },
    filename: function (req, file, cb) {
        console.log('vao fike name', file.fieldname);
        console.log('body', req.body);
        let name_file = file.originalname.split(".")[0] + '-' + Date.now() + path.extname(file.originalname)
        req.body.file = name_file;
        cb(null, name_file);
    }
});

const storage_avatar = multer.diskStorage({
    destination: function (req, file, res) {
        res(null, './static/image/avatar');
    },
    filename: function (req, file, cb) {
        console.log('vao fike name');
        let name_file = file.originalname.split(".")[0] + '-' + Date.now() + path.extname(file.originalname)
        req.body.file = name_file;
        cb(null, name_file);
    }
});


const upload_category = multer({ storage: storage_category })
const upload_product = multer({ storage: storage_product })
const upload_news = multer({ storage: storage_news })
const upload_avatar = multer({ storage: storage_avatar })


uploads.post("/upload-category", upload_category.single('image-category'), upload.uploadCategory);
uploads.post("/upload-product", upload_product.array('image-product', 10), upload.uploadProduct);
uploads.post("/upload-news", upload_news.single('image-news'), upload.uploadNews);
uploads.post("/upload-avatar", upload_avatar.single('image-users'), upload.uploadAvatar);



module.exports = uploads