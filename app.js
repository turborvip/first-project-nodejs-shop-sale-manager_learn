require('dotenv').config();
const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');
let port = process.env.PORT || 3001;
var router = require("./routes");
var upload = require("./uploads");
var cookieSession = require('cookie-session');

app.use(
    cookieSession({
        name: 'session',
        keys: ['key1', 'key2'],
        maxAge: 3600 * 1000 // 1hr
    }
    ))

app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(bodyParser.json()); // support json encoded bodies
// SET OUR VIEWS AND VIEW ENGINE
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, "/static")));

app.use('/', router);
app.use('/', upload);
//CORS
// app.use(cors());
app.use(function (req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
    res.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type, Accept,Authorization,Origin");
    next();
}); `enter code here`


app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
}

);

