const { render } = require('ejs');
const db = require('../../config/db');
const { Op } = require("sequelize");

const category = async (req, res) => {
    const { id } = req.query.id;
    var size = req.query.size || 20;
    var page = req.query.page || 1;
    var query = req.query.query || "";
    var offset = (parseInt(page) - 1) * parseInt(size);
    let detailDB = await db.sequelize.query(
        "SELECT product.id, product.name,product.description,product.image1,product.image2,product.image3,product.unit,product.price,product.status,discount.caption, discount.value FROM`product` left join discount_product on discount_product.id_product = product.id left join discount on discount_product.id_discount = discount.id where product.id_category = " + id + " and product.name like '%" + query + "%' order by product.id desc limit " + size + " offset " + offset + ""
        , { type: db.sequelize.QueryTypes.SELECT }
    );
    data = {
        messenger: 'success',
        status: 201,
        detailDB,
    }
    console.log('data', data);
    return res.render('', { res: true, data });
}
module.exports = {
    category
}