const { render } = require('ejs');
const db = require('../../config/db');
const { Op } = require("sequelize");

const payment = async (req, res) => {
    let provinceDB = await db.sequelize.query(
        "SELECT id,name from province"
        , { type: db.sequelize.QueryTypes.SELECT }
    );
    let data = {
        status: 200,
        msg: "success",
        provinceDB,
    };
    return res.render('client/payment', { res: true, data: data });
}
const getDistrict = async (req, res) => {
    const { idProvince } = req.body;
    let districtDB = await db.sequelize.query(
        "SELECT id,name from district where province =" + idProvince + ""
        , { type: db.sequelize.QueryTypes.SELECT }
    );
    return res.json({
        res: true,
        districtDB,
    })
}
const getWard = async (req, res) => {
    const { idDistrict } = req.body;
    let wardDB = await db.sequelize.query(
        "SELECT id,name from ward where district =" + idDistrict + ""
        , { type: db.sequelize.QueryTypes.SELECT }
    );
    return res.json({
        res: true,
        wardDB,
    })
}

module.exports = {
    payment,
    getDistrict,
    getWard,
}