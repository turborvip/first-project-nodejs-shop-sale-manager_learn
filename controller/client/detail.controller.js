const { render } = require('ejs');
const db = require('../../config/db');
const { Op } = require("sequelize");

const deatailProduct = async (req, res) => {
    const { id } = req.query;
    try {
        let detailDB = await db.sequelize.query(
            "SELECT product.id as product_id,product.name, product.image,product.size,product.color,category.title,category.id as category_id,product.price,product.unit,product.description, discount.caption ,discount.value from product inner join category on product.id_category = category.id left join discount_product on product.id = discount_product.id_product left join discount on discount.id = discount_product.id_discount where category.status = 1 and product.status = 1  and product.id = " + id + ""
            , { type: db.sequelize.QueryTypes.SELECT }
        );

        let otherProductDB = await db.sequelize.query(
            "SELECT product.id,product.name, product.image,product.size,product.color,category.title,product.price,product.unit, discount.caption ,discount.value from product inner join category on product.id_category = category.id left join discount_product on product.id = discount_product.id_product left join discount on discount.id = discount_product.id_discount where category.status = 1 and product.status = 1 order by product.id desc LIMIT 0,20"
            , { type: db.sequelize.QueryTypes.SELECT }
        )
        console.log('detailDB', detailDB);
        data = {
            messenger: 'success',
            status: 201,
            detailDB,
            otherProductDB,
        }
        return res.render('client/detail', { res: true, data });
    }
    catch (err) {
        console.log('err :', err);
    }

}
module.exports = {
    deatailProduct
}