
const { render } = require('ejs');
const db = require('../../config/db');
const { Op } = require("sequelize");

function data_tree(data, parent_id = 0, level = 0) {
    let result = [];
    data.forEach(async function (item, index, array) {
        if (item.parent_id == parent_id) {
            item.level = level;
            result.push(item);
            let child = data_tree(data, item.id, level + 1)
            result = result.concat(child);
        }
    });

    return result;
}
const menu = async (req, res) => {
    let categoryMaleDB = await db.Category.findAll({
        where: {
            [Op.or]: [
                { gender: 1 },
                { gender: 0 }
            ],
            [Op.and]: [
                { status: 1 },
            ]
        },
        attributes: ['id', 'title', 'parent_id'],
    });
    let categoryFeMaleDB = await db.Category.findAll({
        where: {
            [Op.or]: [
                { gender: 2 },
                { gender: 0 }
            ],
            [Op.and]: [
                { status: 1 },
            ]
        },
        attributes: ['id', 'title', 'parent_id'],
    });
    let categoryChildDB = await db.Category.findAll({
        where: {
            [Op.or]: [
                { gender: 3 },
                { gender: 0 }
            ],
            [Op.and]: [
                { status: 1 },
            ]
        },
        attributes: ['id', 'title', 'parent_id'],
    });
    categoryMaleDB = await data_tree(categoryMaleDB);
    categoryFeMaleDB = await data_tree(categoryFeMaleDB);
    categoryChildDB = await data_tree(categoryChildDB);
    let data = {
        status: 200,
        msg: "success",
        categoryMaleDB,
        categoryFeMaleDB,
        categoryChildDB,
    };
    return res.json({ data });
}
module.exports = {
    menu
}