const bcrypt = require('bcryptjs');
const db = require('../../config/db');
const { Op } = require("sequelize");
const { render } = require('ejs');
const jwt = require('jsonwebtoken');

const login = async (req, res) => {
    res.render('client/login');
}

const loginCheck = async (req, res) => {
    const { username, password } = req.body;
    console.log('username :', username);
    console.log('password :', password);
    let userDB = await db.User.findOne({
        where: { user_name: username },
    });
    if (userDB && (await bcrypt.compare(password, userDB.password))) {
        if (userDB.status != 0 && (userDB.role == 1)) {
            console.log('check', 'true')
            return res.json({ res: true, userDB: userDB });
        }
    } else {
        console.log("check", 'false');
        return res.json({ res: false });
    }
}
const register = async (req, res) => {
    const { user, repass } = req.body;
    console.log('username', user);
    console.log('password', repass);
    let userDB = await db.User.findOne({
        where: { user_name: user }
    });
    if (userDB) {
        console.log('check : ', 'false');
        return res.json({ res: false });
    } else {
        console.log('check : ', 'true');
        let encryptedPassword = await bcrypt.hash(repass, 10);
        await db.User.create({
            "user_name": user,
            "password": encryptedPassword,
            "email": '',
            "address": '',
            "role": 1,
            "created_date": new Date()
        });
        return res.json({ res: true, });
    }
}
const logOut = (req, res) => {
    req.session = null;
    return res.redirect('client/loginshop');
}
module.exports = {
    login,
    loginCheck,
    register,
    logOut,
}