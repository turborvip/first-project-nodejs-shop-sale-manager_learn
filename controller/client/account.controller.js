
const { render } = require('ejs');
const db = require('../../config/db');
const { Op } = require("sequelize");
const bcrypt = require('bcryptjs');

const account = async (req, res) => {
    return res.render('client/account', { res: true });
}
const updateInf = (req, res) => {
    const { id, name, email, phone } = req.body;
    db.User.update(
        {
            "name": name,
            "email": email,
            "phone": phone,
            "updated_date": new Date()
        },
        { where: { id: id } },
    );
    return res.json({ res: true });
}
const changepass = async (req, res) => {
    const { id, repass, oldpass } = req.body;
    let encryptedPassword = await bcrypt.hash(repass, 10);
    let userDB = await db.User.findOne(
        {
            where: { id: id },
            attributes: ['password', 'status'],
        });
    console.log('userDB', userDB);
    if (userDB && (await bcrypt.compare(oldpass, userDB.password))) {
        if (userDB.status != 0) {
            db.User.update(
                {
                    "password": encryptedPassword,
                    "updated_date": new Date()
                },
                { where: { id: id } },
            );
            return res.json({ res: true });
        } else {
            return res.json({ res: false });
        }
    } else {
        return res.json({ res: false });
    }
}
const history = async (req, res) => {
    const { id } = req.body;
}
module.exports = {
    account,
    updateInf,
    changepass,
    history,
}