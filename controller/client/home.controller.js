
const { render } = require('ejs');
const db = require('../../config/db');
const { Op } = require("sequelize");

const home = async (req, res) => {
    try {
        let saleDB = await db.sequelize.query(
            "SELECT product.id,product.image, product.name, product.price, product.unit,product.size,product.color, discount.caption, discount.value FROM product inner join discount_product on product.id = discount_product.id_product inner join category on product.id_category = category.id inner join discount on discount_product.id_discount = discount.id where discount.status = 1 and product.status = 1 and category.status = 1 order by discount_product.id desc LIMIT 0,20"
            , { type: db.sequelize.QueryTypes.SELECT }
        );
        let shoesDB = await db.sequelize.query(
            "SELECT product.id,product.name, product.image ,category.title,product.price,product.unit,product.size,product.color, discount.caption ,discount.value from product inner join category on product.id_category = category.id left join discount_product on product.id = discount_product.id_product left join discount on discount.id = discount_product.id_discount where (category.title like '%low%' or category.title like '%high%') and category.status = 1 and product.status = 1 order by product.id desc LIMIT 0,20"
            , { type: db.sequelize.QueryTypes.SELECT }
        );
        let shirtDB = await db.sequelize.query(
            "SELECT product.id,product.name, product.image ,category.title,product.price,product.unit,product.size,product.color, discount.caption ,discount.value from product inner join category on product.id_category = category.id left join discount_product on product.id = discount_product.id_product left join discount on discount.id = discount_product.id_discount where category.title like '%shirt%' and category.status = 1 and product.status = 1 order by product.id desc LIMIT 0,20"
            , { type: db.sequelize.QueryTypes.SELECT }
        );
        let accessoriesDB = await db.sequelize.query(
            "SELECT product.id,product.name, product.image ,category.title,product.price,product.unit,product.size,product.color, discount.caption ,discount.value from product inner join category on product.id_category = category.id left join discount_product on product.id = discount_product.id_product left join discount on discount.id = discount_product.id_discount where category.parent_id = (select category.id from category where category.title like '%accessory%') and category.status = 1 and product.status = 1 order by product.id desc LIMIT 0,20"
            , { type: db.sequelize.QueryTypes.SELECT }
        );
        let newsDB = await db.sequelize.query(
            "SELECT id,caption,description,image,author,created_date FROM news where status = 1 order by news.id desc LIMIT 0,20"
            , { type: db.sequelize.QueryTypes.SELECT }
        );
        let data = {
            status: 200,
            msg: "success",
            shirtDB,
            saleDB,
            shoesDB,
            newsDB,
            accessoriesDB,
        };
        // res.json({ data });
        return res.render('client/home', { title: "List Product", data: data });

    } catch (err) {
        console.log("err : ", err);
        let data = {
            status: 500,
            msg: "errr",
        };
        res.json({ data });
    }

}





module.exports = {
    home,
}