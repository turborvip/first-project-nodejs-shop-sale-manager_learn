const { render } = require('ejs');
const db = require('../../config/db');
const { Op } = require("sequelize");

const news = async (req, res) => {
    const { id } = req.query;
    try {
        let newsDB = await db.sequelize.query(
            "SELECT news.id,news.caption,news.description,news.content,news.image,news.author,news.created_date FROM news where news.id = " + id + ""
            , { type: db.sequelize.QueryTypes.SELECT }
        );
        let allNewsDB = await db.sequelize.query(
            "SELECT id,caption,description,image,author,created_date FROM news where status = 1 order by news.id desc LIMIT 0,20"
            , { type: db.sequelize.QueryTypes.SELECT }
        );
        console.log('newsDB :', newsDB);
        data = {
            messenger: 'success',
            status: 201,
            newsDB,
            allNewsDB,
        }
        return res.render('client/news', { res: true, data });
    }
    catch (err) {
        console.log('err :', err);
    }

}
module.exports = {
    news
}