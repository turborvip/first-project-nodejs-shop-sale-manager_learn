const { render } = require('ejs');
const db = require('../../config/db');
const { Op } = require("sequelize");

const product = async (req, res) => {
    var category = req.query.idCategory;
    var size = req.query.size || 20;
    var page = req.query.page || 1;
    var query = req.query.query || "";
    var offset = (parseInt(page) - 1) * parseInt(size);
    try {
        let ProductDB = await db.sequelize.query(
            "SELECT product.id , product.name,product.description,product.image,product.unit,product.price,product.size,product.color,category.id as id_category,discount.caption ,discount.value FROM`product` inner join`category` on product.id_category = category.id left join discount_product on product.id = discount_product.id_product left join discount on discount.id = discount_product.id_discount where (category.id = " + category + " or category.parent_id = " + category + ") and  product.name like '%" + query + "%' order by product.id desc limit " + size + " offset " + offset + ""
            , { type: db.sequelize.QueryTypes.SELECT }
        );
        console.log('ProductDB', ProductDB);
        let countProduct = await db.Product.count({
            where: {
                [Op.or]: [
                    {
                        name: {
                            [Op.like]: `%${query}%`
                        }
                    },
                    {
                        description: {
                            [Op.like]: `%${query}%`
                        }
                    },

                ],
            },

        });
        let totalPage = Math.ceil(countProduct / parseInt(size))
        let fullUrl = req.protocol + '://' + req.get('host');

        let data = {
            status: 200,
            msg: "success",
            ProductDB,
            totalPage,
            category,
            fullUrl,
            page,
            query,
        };
        // console.log('product : ', data);
        return res.render('client/products by category', { title: "List Product", data: data });
    } catch (err) {
        let data = {
            status: 500,
            msg: "errr",
        };
        return res.render('client/products by category', { title: "List Product", data: data });
    }
}
module.exports = {
    product,
}