const path = require('path');
const fs = require('fs');

const uploadCategory = async (req, res) => {
    const { file } = req.body;
    const { nameOldFile } = req.body || null;
    console.log('file', file);
    console.log('nameOldFile', nameOldFile);
    if (nameOldFile != null) {
        fs.unlink('./static/image/category/' + nameOldFile, function (err) {
            if (err) {
                console.error(err);
            } else {
                console.log('File has been Deleted');
            }
        });
    }
    let data = {
        status: 200,
        msg: "success",
        file: file,
    };
    return res.json({ res: true, data });
}
const uploadProduct = async (req, res) => {
    const { file } = req.body;
    console.log('file2', file);
    let nameOldFiles = null;
    if (req.body.nameOldFiles) {
        nameOldFiles = JSON.parse(req.body.nameOldFiles);
        console.log('nameOldFiles', nameOldFiles);
        console.log('nameOldFiles length', nameOldFiles.length);
    }

    if (nameOldFiles != null) {
        for (let i = 0; i < nameOldFiles.length; i++) {
            fs.unlink('./static/image/product/' + nameOldFiles[i], function (err) {
                if (err) {
                    console.error(err);
                } else {
                    console.log('File has been Deleted');
                }
            });
        }
    }
    let data = {
        status: 200,
        msg: "success",
        file: file,
    };
    return res.json({ res: true, data });
}
const uploadNews = async (req, res) => {
    const { file } = req.body;
    const { nameOldFile } = req.body || null;
    console.log('file', file);
    console.log('nameOldFile', nameOldFile);
    if (nameOldFile != null) {
        fs.unlink('./static/image/news/' + nameOldFile, function (err) {
            if (err) {
                console.error(err);
            } else {
                console.log('File has been Deleted');
            }
        });
    }
    let data = {
        status: 200,
        msg: "success",
        file: file,
    };
    return res.json({ res: true, data });
}
const uploadAvatar = async (req, res) => {
    const { file } = req.body;
    console.log('vào upload', res);
    console.log('file', file);
    let data = {
        status: 200,
        msg: "success",
        file: req.body.file,
    };
    return res.json({ res: true, data });
}

module.exports = {
    uploadCategory,
    uploadProduct,
    uploadNews,
    uploadAvatar
}