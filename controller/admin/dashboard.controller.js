const { render } = require('ejs');

const dashboard = (req, res) => {
    let dataUser = req.infoUser;
    console.log(' vào dashboard');
    return res.render('admin/dashboard', { title: "Home", user: dataUser });
}

module.exports = {
    dashboard,
}