const { render } = require('ejs');
const db = require('../../config/db');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const login = async (req, res) => {
    return res.render('admin/login');
}

const loginCheck = async (req, res) => {
    const { username, password } = req.body;
    let userDB = await db.User.findOne({
        where: { user_name: username },
    });
    await bcrypt.compare(password, userDB.password).then(function (result) {
        let check = result;
        if (userDB && check) {
            if (userDB.role == 2 || userDB.role == 3) {
                console.log("check", 'true');
                const accessToken = jwt.sign({ data: userDB }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '1d' });
                console.log("accessToken", accessToken)
                res.cookie('token', `${accessToken}`, { expires: new Date(Date.now() + 5184000) });
                return res.json({ res: true, user: userDB, accessToken });
            } else {
                console.log("check", 'false');
                return res.json({ res: false });
            }
        } else {
            console.log("check", 'false');
            return res.json({ res: false });
        }
    });

}
const logOut = (req, res) => {
    res.clearCookie('token');
    return res.redirect('/loginshop');
}
module.exports = {
    login,
    loginCheck,
    logOut,
}