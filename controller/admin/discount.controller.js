const { render } = require('ejs');
const db = require('../../config/db');
const { Op } = require("sequelize");

const deleteDiscountProduct = async (req, res) => {
    const { id } = req.body;
    await db.Discount_product.destroy({
        where: { id: id },
    });
    res.send({ title: "List Discount - Product", res: true });
}
const addDiscountProductPost = async (req, res) => {
    const { idDiscount, idProduct } = req.body;
    console.log('idDiscount :', idDiscount);
    console.log('idProduct :', idProduct);

    let DiscountProductDB = await db.Discount_product.findOne({
        where: {
            id_product: idProduct,
        }
    });
    console.log('DiscountProductDB :', DiscountProductDB);
    if (DiscountProductDB) {
        console.log('discount already exist!')
        return res.json({ res: false });
    } else {
        await db.Discount_product.create({
            "id_product": idProduct,
            "id_discount": idDiscount,
            "created_date": new Date()
        });
        return res.json({ res: true });
    }
}
const addDiscountProduct = async (req, res) => {
    console.log('đã vào addDiscountProduct');
    const { id } = req.params;
    let DiscountDB = await db.Discount.findOne({
        where: { id: id },
        attributes: ['id', 'caption',],
    });
    var size = req.query.size || 20;
    var page = req.query.page || 1;
    var query = req.query.query || "";
    var dateFrom = req.query.dateFrom || new Date();
    var dateTo = req.query.dateTo || new Date("2010-12-12 00:00:00");
    var offset = (parseInt(page) - 1) * parseInt(size);

    try {

        let ProductDB = await db.sequelize.query(
            "SELECT product.id, product.name,category.title FROM`product` inner join`category` on product.id_category = category.id where name like '%" + query + "%' or product.description like '%" + query + "%' and product.created_date between '" + dateFrom + "'  and '" + dateTo + "' order by id desc limit " + size + " offset " + offset + ""
            , { type: db.sequelize.QueryTypes.SELECT }
        );
        let countProduct = await db.Product.count({
            where: {
                [Op.or]: [
                    {
                        name: {
                            [Op.like]: `%${query}%`
                        }
                    },
                    {
                        description: {
                            [Op.like]: `%${query}%`
                        }
                    },

                ],
                created_date: {
                    [Op.between]: [dateTo, dateFrom]
                },
            },

        });
        let totalPage = Math.ceil(countProduct / parseInt(size))
        let fullUrl = req.protocol + '://' + req.get('host');

        let data = {
            status: 200,
            msg: "success",
            ProductDB,
            totalPage,
            fullUrl,
            page,
            query,
            DiscountDB,
            token: "Bearer " + req.token,
        };
        return res.render('admin/discount/useDiscount', { title: "Use", data: data });
    } catch (err) {
        let data = {
            status: 500,
            msg: "errr",
            CategoryDB,
            totalPage
        };
        return res.render('admin/discount/useDiscount', { title: "Use", data: data });
    }
}
const listDiscountProduct = async (req, res) => {
    var size = req.query.size || 20;
    var page = req.query.page || 1;
    var query = req.query.query || "";
    var dateTo = req.query.dateFrom || new Date();
    var dateFrom = req.query.dateTo || new Date("2010-12-12 00:00:00");
    var offset = (parseInt(page) - 1) * parseInt(size);
    console.log('query :', query);
    try {
        let Discount_ProductDB = await db.sequelize.query(
            "SELECT discount_product.id,discount_product.id_product,discount_product.id_discount,product.name,discount.caption,discount.status FROM `discount_product` inner join`product` on product.id = discount_product.id_product inner join`discount` on discount.id = discount_product.id_discount  where discount.caption like '%" + query + "%' or product.name like '%" + query + "%' order by discount_product.id desc limit " + size + " offset " + offset + ""
            , { type: db.sequelize.QueryTypes.SELECT }
        );

        let countDiscount_Product = await db.sequelize.query(
            "SELECT count(id_product) as soluong FROM `discount_product` inner join`product` on product.id = discount_product.id_product inner join`discount` on discount.id = discount_product.id_discount  where discount.caption like '%" + query + "%' or product.name like '%" + query + "%' and discount_product.created_date between '" + dateFrom + "'  and '" + dateTo + "' order by discount_product.id desc limit " + size + " offset " + offset + ""
            , { type: db.sequelize.QueryTypes.SELECT }
        );

        console.log('countDiscount_Product :', countDiscount_Product[0].soluong);
        console.log('Discount_ProductDB :', Discount_ProductDB);
        let totalPage = Math.ceil(countDiscount_Product[0].soluong / parseInt(size))
        let fullUrl = req.protocol + '://' + req.get('host');

        let data = {
            status: 200,
            msg: "success",
            Discount_ProductDB,
            totalPage,
            fullUrl,
            page,
            query,
            token: "Bearer " + req.token,
        };
        console.log('data : ', data);
        return res.render('admin/discount/listDiscountProduct', { title: "List Discount - Product", data: data });
    } catch (err) {
        let data = {
            status: 500,
            msg: "errr",
            totalPage
        };
        return res.render('admin/discount/listDiscountProduct', { title: "List Discount - Product", data });
    }
}
const listDiscount = async (req, res) => {

    var size = req.query.size || 20;
    var page = req.query.page || 1;
    var query = req.query.query || "";
    var dateFrom = req.query.dateFrom || new Date();
    var dateTo = req.query.dateTo || new Date("2010-12-12 00:00:00");
    var offset = (parseInt(page) - 1) * parseInt(size);
    console.log('query :', query);
    try {
        let DiscountDB = await db.Discount.findAll({
            where: {
                [Op.or]: [
                    {
                        caption: {
                            [Op.like]: `%${query}%`
                        }
                    },
                    {
                        description: {
                            [Op.like]: `%${query}%`
                        }
                    },

                ],
                created_date: {
                    [Op.between]: [dateTo, dateFrom]
                },
            },
            offset: offset,
            limit: parseInt(size),
            order: [
                ['id', 'DESC'],
            ],
        });

        let countDiscount = await db.Discount.count({
            where: {
                [Op.or]: [
                    {
                        caption: {
                            [Op.like]: `%${query}%`
                        }
                    },
                    {
                        description: {
                            [Op.like]: `%${query}%`
                        }
                    },

                ],
                created_date: {
                    [Op.between]: [dateTo, dateFrom]
                },
            },

        });
        let totalPage = Math.ceil(countDiscount / parseInt(size))
        let fullUrl = req.protocol + '://' + req.get('host');

        let data = {
            status: 200,
            msg: "success",
            DiscountDB,
            totalPage,
            fullUrl,
            page,
            query,
            token: "Bearer " + req.token,
        };
        console.log('Discount : ', data);
        return res.render('admin/discount/listDiscount', { title: "List Discount", data: data });
    } catch (err) {
        let data = {
            status: 500,
            msg: "errr",
            DiscountDB,
            totalPage
        };
        return res.render('admin/discount/listDiscount', { title: "List Discount", data });
    }

}
const activeDiscount = async (req, res) => {
    const { id } = req.body;
    let statusUpdate;
    let DiscountDB = await db.Discount.findOne({
        where: { id: id },
        attributes: ['status'],
    });
    if (DiscountDB.status == 1) {
        statusUpdate = 0;
    } else {
        statusUpdate = 1;
    }
    console.log('status :', statusUpdate);
    await db.Discount.update(
        {
            status: statusUpdate
        },
        { where: { id: id } },
    );
    res.send({ title: "List Discount", res: true });
}
const deleteDiscount = async (req, res) => {
    const { id } = req.body;
    await db.Discount.destroy({
        where: { id: id },
    });
    res.send({ title: "List Discount", res: true });
}
const updateDiscount = async (req, res) => {
    const { id } = req.params;
    console.log("id update :", id);
    let DiscountUpdate = await db.Discount.findOne({
        where: { id: id },
        attributes: ['id', 'caption', 'description', 'value', 'author'],
    });
    return res.render('admin/discount/updateDiscount', { title: "Update Discount", data: DiscountUpdate });
}
const updateDiscountPost = async (req, res) => {
    const { id, caption, description, value, author } = req.body;
    console.log("id update :", id);
    console.log("caption :", caption);
    console.log("description update :", description);
    console.log("value:", value);
    console.log("name update :", author);
    let nameDiscountDB = await db.Discount.findOne({
        where: { id: id },
        attributes: ['caption']
    });
    console.log("nameDiscountDB :", nameDiscountDB.title);
    let DiscountDB = await db.Discount.findOne({
        where: {
            caption: caption,
            [Op.not]: { caption: nameDiscountDB.caption }
        },
        attributes: ['caption']
    });

    console.log("DiscountDB :", DiscountDB);
    if (DiscountDB) {
        return res.json({ res: false });
    } else {
        await db.Discount.update(
            {
                "caption": caption,
                "description": description,
                "value": value,
                "author": author,
                "updated_date": new Date()
            },
            { where: { id: id } },
        );
        return res.json({ res: true });
    }
}
const createDiscount = (req, res) => {
    res.render('admin/discount/createDiscount', { title: "Create Discount" })
}
const createDiscountPost = async (req, res) => {
    const { caption, description, value, author } = req.body;
    console.log('caption :', caption);
    console.log('description :', description);
    console.log('value :', value);
    console.log('author :', author);
    let DiscountDB = await db.Discount.findOne({
        where: { caption: caption }
    });
    console.log('DiscountDB :', DiscountDB);
    if (DiscountDB) {
        return res.json({ res: false });
    } else {
        await db.Discount.create({
            "caption": caption,
            "description": description,
            "value": value,
            "author": author,
            "status": 0,
            "created_date": new Date()
        });
        return res.json({ res: true });
    }
}
module.exports = {
    deleteDiscountProduct,
    addDiscountProductPost,
    addDiscountProduct,
    listDiscountProduct,
    createDiscountPost,
    createDiscount,
    activeDiscount,
    deleteDiscount,
    updateDiscount,
    updateDiscountPost,
    listDiscount,
}