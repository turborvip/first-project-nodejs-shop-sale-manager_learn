const { render } = require('ejs');
const db = require('../../config/db');
const { Op } = require("sequelize");

const listNews = async (req, res) => {

    var size = req.query.size || 20;
    var page = req.query.page || 1;
    var query = req.query.query || "";
    var dateFrom = req.query.dateFrom || new Date();
    var dateTo = req.query.dateTo || new Date("2010-12-12 00:00:00");
    var offset = (parseInt(page) - 1) * parseInt(size);
    console.log('query :', query);
    try {
        let NewsDB = await db.News.findAll({
            where: {
                [Op.or]: [
                    {
                        caption: {
                            [Op.like]: `%${query}%`
                        }
                    },
                    {
                        description: {
                            [Op.like]: `%${query}%`
                        }
                    },

                ],
                created_date: {
                    [Op.between]: [dateTo, dateFrom]
                },
            },
            offset: offset,
            limit: parseInt(size),
            order: [
                ['id', 'DESC'],
            ],
        });

        let countNews = await db.News.count({
            where: {
                [Op.or]: [
                    {
                        caption: {
                            [Op.like]: `%${query}%`
                        }
                    },
                    {
                        description: {
                            [Op.like]: `%${query}%`
                        }
                    },

                ],
                created_date: {
                    [Op.between]: [dateTo, dateFrom]
                },
            },

        });
        let totalPage = Math.ceil(countNews / parseInt(size))
        let fullUrl = req.protocol + '://' + req.get('host');

        let data = {
            status: 200,
            msg: "success",
            NewsDB,
            totalPage,
            fullUrl,
            page,
            query,
            token: "Bearer " + req.token,
        };
        console.log('News : ', data);
        return res.render('admin/news/listNews', { title: "List News", data: data });
    } catch (err) {
        let data = {
            status: 500,
            msg: "errr",
            NewsDB,
            totalPage
        };
        return res.render('admin/news/listNews', { title: "List News", data });
    }

}
const activeNews = async (req, res) => {
    const { id } = req.body;
    let statusUpdate;
    let NewsDB = await db.News.findOne({
        where: { id: id },
        attributes: ['status'],
    });
    if (NewsDB.status == 1) {
        statusUpdate = 0;
    } else {
        statusUpdate = 1;
    }
    console.log('status :', statusUpdate);
    await db.News.update(
        {
            status: statusUpdate
        },
        { where: { id: id } },
    );
    res.send({ title: "List News", res: true });
}
const deleteNews = async (req, res) => {
    const { id } = req.body;
    await db.News.destroy({
        where: { id: id },
    });
    res.send({ title: "List News", res: true });
}
const updateNews = async (req, res) => {
    const { id } = req.params;
    console.log("id update :", id);
    let NewsUpdate = await db.News.findOne({
        where: { id: id },
        attributes: ['id', 'caption', 'image', 'description', 'author', 'content'],
    });
    return res.render('admin/news/updateNews', { title: "Update News", data: NewsUpdate });
}
const updateNewsPost = async (req, res) => {
    const { id, caption, description, image_news, content, author } = req.body;
    console.log("id update :", id);
    console.log("image update :", caption);
    console.log("description update :", description);
    console.log("gender update :", content);
    console.log("name update :", author);
    let nameNewsDB = await db.News.findOne({
        where: { id: id },
        attributes: ['caption']
    });
    console.log("nameNewsDB :", nameNewsDB.title);
    let NewsDB = await db.News.findOne({
        where: {
            caption: caption,
            [Op.not]: { caption: nameNewsDB.caption }
        },
        attributes: ['caption']
    });

    console.log("NewsDB :", NewsDB);
    if (NewsDB) {
        return res.json({ res: false });
    } else {
        await db.News.update(
            {
                "caption": caption,
                "description": description,
                "content": content,
                "author": author,
                "image": image_news,
                "updated_date": new Date()
            },
            { where: { id: id } },
        );
        return res.json({ res: true });
    }
}
const createNews = (req, res) => {
    res.render('admin/news/createNews', { title: "Create News" })
}
const createNewsPost = async (req, res) => {
    const { caption, description, content, author, image_news } = req.body;
    console.log('caption :', caption);
    console.log('description :', description);
    console.log('content :', content);
    console.log('author :', author);
    console.log('image :', image_news);

    let NewsDB = await db.News.findOne({
        where: { caption: caption }
    });
    console.log('NewsDB :', NewsDB);
    if (NewsDB) {
        return res.json({ res: false });
    } else {
        await db.News.create({
            "caption": caption,
            "description": description,
            "content": content,
            "author": author,
            "image": image_news,
            "status": 0,
            "created_date": new Date()
        });
        return res.json({ res: true });
    }
}
module.exports = {
    createNewsPost,
    createNews,
    activeNews,
    deleteNews,
    updateNews,
    updateNewsPost,
    listNews,
}