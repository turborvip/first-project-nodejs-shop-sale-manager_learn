const { render } = require('ejs');
const db = require('../../config/db');
const { Op } = require("sequelize");

const listCategory = async (req, res) => {

    var size = req.query.size || 20;
    var page = req.query.page || 1;
    var query = req.query.query || "";
    var dateFrom = req.query.dateFrom || new Date();
    var dateTo = req.query.dateTo || new Date("2010-12-12 00:00:00");
    var offset = (parseInt(page) - 1) * parseInt(size);
    console.log('query :', query);
    try {
        let CategoryDB = await db.Category.findAll({
            where: {
                [Op.or]: [
                    {
                        title: {
                            [Op.like]: `%${query}%`
                        }
                    },
                    {
                        description: {
                            [Op.like]: `%${query}%`
                        }
                    },

                ],
                created_date: {
                    [Op.between]: [dateTo, dateFrom]
                },
            },
            offset: offset,
            limit: parseInt(size),
            order: [
                ['id', 'DESC'],
            ],
        });

        let countCategory = await db.Category.count({
            where: {
                [Op.or]: [
                    {
                        title: {
                            [Op.like]: `%${query}%`
                        }
                    },
                    {
                        description: {
                            [Op.like]: `%${query}%`
                        }
                    },

                ],
                created_date: {
                    [Op.between]: [dateTo, dateFrom]
                },
            },

        });
        let totalPage = Math.ceil(countCategory / parseInt(size))
        let fullUrl = req.protocol + '://' + req.get('host');

        let data = {
            status: 200,
            msg: "success",
            CategoryDB,
            totalPage,
            fullUrl,
            token: "Bearer " + req.token,
            page,
            query,
        };
        console.log('category : ', data);
        return res.render('admin/category/listCategory', { title: "List Category", data: data });
    } catch (err) {
        let data = {
            status: 500,
            msg: "errr",
            CategoryDB,
            totalPage
        };
        return res.render('admin/category/listCategory', { title: "List Category", data });
    }

}
const activeCategory = async (req, res) => {
    const { id } = req.body;
    let statusUpdate;
    let CategoryDB = await db.Category.findOne({
        where: { id: id },
        attributes: ['status'],
    });
    if (CategoryDB.status == 1) {
        statusUpdate = 0;
    } else {
        statusUpdate = 1;
    }
    console.log('status :', statusUpdate);
    await db.Category.update(
        {
            status: statusUpdate
        },
        { where: { id: id } },
    );
    res.send({ title: "List Category", res: true });
}
const deleteCategory = async (req, res) => {
    const { id } = req.body;
    await db.Category.destroy({
        where: { id: id },
    });
    res.send({ title: "List Category", res: true });
}
const updateCategory = async (req, res) => {
    const { id } = req.params;
    console.log("id update :", id);
    let CategoryDB = await db.Category.findAll({
        attributes: ['id', 'title'],
    });
    let CategoryUpdate = await db.Category.findOne({
        where: { id: id },
        attributes: ['parent_id', 'title', 'description', 'image', 'gender', 'id'],
    });
    let ParentCategory = await db.Category.findOne({
        where: { id: CategoryUpdate.parent_id },
        attributes: ['title', 'id'],
    });
    console.log('ParentCategory', ParentCategory);
    return res.render('admin/category/updateCategory', { title: "Update Category", data: { CategoryUpdate, CategoryDB, ParentCategory } });
}
const updateCategoryPost = async (req, res) => {
    const { id, parent_category, image_category, description, gender, name } = req.body;
    let nameCategoryDB = await db.Category.findOne({
        where: { id: id },
        attributes: ['id']
    });
    console.log("nameCategoryDB :", nameCategoryDB.title);
    let CategoryDB = await db.Category.findOne({
        where: {
            [Op.and]: [
                { title: name },
                { gender: gender }
            ],
            [Op.not]: { id: nameCategoryDB.id }
        },
        attributes: ['title']
    });
    if (CategoryDB) {
        return res.json({ res: false });
    } else {
        await db.Category.update(
            {
                "parent_id": parent_category,
                "title": name,
                "description": description,
                "gender": gender,
                "image": image_category,
                "updated_date": new Date()
            },
            { where: { id: id } },
        );
        return res.json({ res: true });
    }
}
const createCategory = async (req, res) => {
    let CategoryDB = await db.Category.findAll({
        attributes: ['id', 'title'],
    });
    res.render('admin/category/createCategory', { title: "Create Category", data: { CategoryDB } })
}
const createCategoryPost = async (req, res) => {
    const { parent_category, name, description, image_category, gender } = req.body;
    console.log('image :', image_category)
    let CategoryDB = await db.Category.findOne({
        where: {
            [Op.and]: [
                { title: name },
                { gender: gender }
            ]
        },
    });
    if (CategoryDB) {
        return res.json({ res: false });
    } else {
        await db.Category.create({
            "parent_id": parent_category,
            "title": name,
            "description": description,
            "gender": gender,
            "image": image_category,
            "status": 0,
            "created_date": new Date()
        });
        return res.json({ res: true });
    }
}
module.exports = {
    createCategoryPost,
    createCategory,
    activeCategory,
    deleteCategory,
    updateCategory,
    updateCategoryPost,
    listCategory,
}