const { render } = require('ejs');
const db = require('../../config/db');
const { Op } = require("sequelize");

const listOrder = async (req, res) => {
    var size = req.query.size || 20;
    var page = req.query.page || 1;
    var query = req.query.query || "";
    var dateFrom = req.query.dateFrom || new Date();
    var dateTo = req.query.dateTo || new Date("2010-12-12 00:00:00");
    var offset = (parseInt(page) - 1) * parseInt(size);
    console.log('query :', query);
    try {
        let OrderDB = await db.Order.findAll({
            where: {
                [Op.or]: [
                    {
                        code: {
                            [Op.like]: `%${query}%`
                        }
                    },
                    {
                        fullname: {
                            [Op.like]: `%${query}%`
                        }
                    },
                ],
                created_date: {
                    [Op.between]: [dateTo, dateFrom]
                },
            },
            offset: offset,
            limit: parseInt(size),
            order: [
                ['id', 'DESC'],
            ],
        });

        let countOrder = await db.Order.count({
            where: {
                [Op.or]: [
                    {
                        code: {
                            [Op.like]: `%${query}%`
                        }
                    },
                    {
                        fullname: {
                            [Op.like]: `%${query}%`
                        }
                    },

                ],
                created_date: {
                    [Op.between]: [dateTo, dateFrom]
                },
            },

        });
        let totalPage = Math.ceil(countOrder / parseInt(size))
        let fullUrl = req.protocol + '://' + req.get('host');

        let data = {
            status: 200,
            msg: "success",
            OrderDB,
            totalPage,
            fullUrl,
            page,
            query,
            token: "Bearer " + req.token,
        };
        console.log('Order : ', data);
        return res.render('admin/order/listOrder', { title: "List Order", data: data });
    } catch (err) {
        let data = {
            status: 500,
            msg: "errr",
            OrderDB,
            totalPage
        };
        return res.render('admin/order/listOrder', { title: "List Order", data });
    }

}
const statusOrder = async (req, res) => {
    const { id } = req.body;
    let statusUpdate;
    let OrderDB = await db.Order.findOne({
        where: { id: id },
        attributes: ['status'],
    });
    if (OrderDB.status == 0) {
        statusUpdate = 1;
        await db.Order.update(
            {
                status: statusUpdate
            },
            { where: { id: id } },
        );
        return res.send({ title: "List Order", res: true });
    } else {
        return res.send({ title: "List Order", res: false });
    }
    console.log('status :', statusUpdate);

}
const deleteOrder = async (req, res) => {
    const { id } = req.body;
    await db.Order.destroy({
        where: { id: id },
    });
    res.send({ title: "List Order", res: true });
}
const detailOrder = async (req, res) => {
    const { id } = req.query;
    console.log("id order :", id);
    let orderCode = await db.Order.findOne({
        where: { id: id },
        attributes: ['code'],
    });
    let orderDetail = await db.sequelize.query(
        "SELECT product.name,product.image,order_product.quantity,order_product.color,order_product.size FROM`product` inner join`order_product` on product.id = order_product.id_product inner join`order` on order.id = order_product.id_order where order.id =" + id + " "
        , { type: db.sequelize.QueryTypes.SELECT }
    );
    console.log("orderCode :", orderCode.code);
    console.log("orderDetailDB :", orderDetail);
    return res.render('admin/order/detailOrder', { title: "Code : " + orderCode.code, data: orderDetail });
}
const createOrder = async (req, res) => {
    console.log('vào createOrder');
    let today = new Date();
    let date = today.getHours() + '' + today.getMinutes() + '' + today.getSeconds() + '' + today.getDate() + '' + (today.getMonth() + 1) + '' + today.getFullYear();
    let codeOrder = Math.random().toString(36).substring(7) + date;
    const { fullname, recipients, phonenumber, method, total, description, dataCart } = req.body;
    // dataCart =
    //     [
    //         { "id_product": 1, "quantity": 2, "price": 2000, },
    //         { "id_product": 2, "quantity": 3, "price": 3000, }
    //     ];
    let orderDB = await db.Order.create({
        "code": codeOrder,
        "fullname": fullname,
        "address": recipients,
        "phone": phonenumber,
        "method": method,
        "total": total,
        "description": description,
        "status": 0,
        "created_date": new Date(),
    });
    console.log('orderDB', orderDB.id);
    dataCart.forEach(async function (cartValue, index, array) {
        await db.Order_product.create({
            "id_order": orderDB.id,
            "id_product": cartValue["id_product"],
            "color": cartValue["color"],
            "size": cartValue["size"],
            "quantity": cartValue["quantity"],
        });
    });
    return res.json({ res: true });
}
module.exports = {
    listOrder,
    statusOrder,
    deleteOrder,
    detailOrder,
    createOrder,
}