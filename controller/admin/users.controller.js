const { render } = require('ejs');
const db = require('../../config/db');
const { Op } = require("sequelize");
const bcrypt = require('bcryptjs');

const listUsers = async (req, res) => {
    // console.log('aaaa đã vào');
    var size = req.query.size || 20;
    var page = req.query.page || 1;
    var query = req.query.query || "";
    var dateFrom = req.query.dateFrom || new Date();
    var dateTo = req.query.dateTo || new Date("2010-12-12 00:00:00");
    var offset = (parseInt(page) - 1) * parseInt(size);
    console.log('query :', query);
    try {
        let UsersDB = await db.User.findAll({
            where: {
                [Op.or]: [
                    {

                        name: {
                            [Op.like]: `%${query}%`
                        }
                    },
                    {
                        user_name: {
                            [Op.like]: `%${query}%`
                        }
                    },
                    {
                        description: {
                            [Op.like]: `%${query}%`
                        },
                    },

                ],
                created_date: {
                    [Op.between]: [dateTo, dateFrom]
                },
            },
            offset: offset,
            limit: parseInt(size),
            order: [
                ['id', 'DESC'],
            ],
        });

        let countUsers = await db.User.count({
            where: {
                [Op.or]: [
                    {
                        user_name: {
                            [Op.like]: `%${query}%`
                        }
                    },
                    {
                        name: {
                            [Op.like]: `%${query}%`
                        }
                    },

                ],
                created_date: {
                    [Op.between]: [dateTo, dateFrom]
                },
            },

        });
        let totalPage = Math.ceil(countUsers / parseInt(size))
        let fullUrl = req.protocol + '://' + req.get('host');

        let data = {
            status: 200,
            msg: "success",
            UsersDB,
            User: req.infoUser,
            token: "Bearer " + req.token,
            totalPage,
            fullUrl,
            page,
            query,
        };
        console.log('Users : ', data);
        return res.render('admin/users/listUsers', { title: "List Users", data: data });
    } catch (err) {
        let data = {
            status: 500,
            msg: "errr",
            UsersDB,
            totalPage
        };
        return res.render('admin/users/listUsers', { title: "List Users", data });
    }

}
const activeUsers = async (req, res) => {
    const { id } = req.body;
    let statusUpdate;
    let UsersDB = await db.User.findOne({
        where: { id: id },
        attributes: ['status'],
    });
    if (UsersDB.status == 1) {
        statusUpdate = 0;
    } else {
        statusUpdate = 1;
    }
    console.log('status :', statusUpdate);
    await db.User.update(
        {
            status: statusUpdate
        },
        { where: { id: id } },
    );
    res.send({ title: "List Users", res: true });
}
const deleteUsers = async (req, res) => {
    const { id } = req.body;
    await db.User.destroy({
        where: { id: id },
    });
    res.send({ title: "List Users", res: true });
}
const updateUsers = async (req, res) => {
    const { id, name, email, address, description, image_users, phone } = req.body;
    console.log("id update :", id);
    console.log("image update :", image_users);
    console.log("description update :", description);
    console.log("address update :", address);
    console.log("name update :", name);
    console.log("email update :", email);
    await db.User.update(
        {
            "name": name,
            "description": description,
            "email": email,
            "phone": phone,
            "address": address,
            "image": image_users,
            "updated_date": new Date()
        },
        { where: { id: id } },
    );
    return res.json({ res: true });
}
const changePass = async (req, res) => {
    const { id, oldpass, repass } = req.body;
    console.log(id, oldpass, repass);
    let userDB = await db.User.findOne({
        where: { id: id },
        attributes: ['password'],
    });
    let match = await bcrypt.compare(oldpass, userDB.password);
    let encryptedPassword = await bcrypt.hash(repass, 10);
    console.log("userDB", userDB.password);
    console.log('match', match);
    if (match) {
        await db.User.update(
            {
                "password": encryptedPassword,
                "updated_date": new Date()
            },
            { where: { id: id } },
        );
        return res.json({ res: true });
    } else {
        return res.json({ res: false });
    }
}
const createUsers = (req, res) => {
    res.render('admin/users/createUsers', { title: "Create Users" })
}
const createUsersPost = async (req, res) => {
    const { username, name, email, address, description, re_pass, image_users, role, phone } = req.body;
    let encryptedPassword = await bcrypt.hash(re_pass, 10);
    // console.log('encryptedPassword :', encryptedPassword)
    // console.log('username :', username)
    // console.log('email :', email)
    // console.log('address :', address)
    // console.log('description :', description)
    // console.log('image :', image)
    // console.log('role :', role)
    // console.log('re_pass :', re_pass)
    let UsersDB = await db.User.findOne({
        where: { user_name: username }
    });
    if (UsersDB) {
        return res.json({ res: false });
    } else {
        await db.User.create({
            "user_name": username,
            "description": description,
            "password": encryptedPassword,
            "name": name,
            "email": email,
            "phone": phone,
            "description": description,
            "address": address,
            "image": image_users,
            "role": role,
            "status": 0,
            "created_date": new Date()
        });
        return res.json({ res: true });
    }
}
module.exports = {
    createUsersPost,
    createUsers,
    activeUsers,
    deleteUsers,
    updateUsers,
    changePass,
    listUsers,
}