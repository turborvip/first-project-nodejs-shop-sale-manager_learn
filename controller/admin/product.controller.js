const { render } = require('ejs');
const db = require('../../config/db');
const { Op } = require("sequelize");
const { model } = require('../../models/user.model');

// Product
const listProduct = async (req, res) => {
    let dataUser = req.infoUser
    console.log("dataUser", dataUser)
    var size = req.query.size || 20;
    var page = req.query.page || 1;
    var query = req.query.query || "";
    var dateFrom = req.query.dateFrom || new Date();
    var dateTo = req.query.dateTo || new Date("2010-12-12 00:00:00");
    var offset = (parseInt(page) - 1) * parseInt(size);

    try {

        let ProductDB = await db.sequelize.query(
            "SELECT product.id, product.name,product.description,category.gender,product.image,product.unit,product.price,product.size,product.color,product.status,category.title FROM`product` inner join`category` on product.id_category = category.id where product.name like '%" + query + "%' or product.description like '%" + query + "%' and (product.created_date between '" + dateFrom + "'  and '" + dateTo + "') order by product.id desc limit " + size + " offset " + offset + ""
            , { type: db.sequelize.QueryTypes.SELECT }
        );
        let countProduct = await db.Product.count({
            where: {
                [Op.or]: [
                    {
                        name: {
                            [Op.like]: `%${query}%`
                        }
                    },
                    {
                        description: {
                            [Op.like]: `%${query}%`
                        }
                    },

                ],
                created_date: {
                    [Op.between]: [dateTo, dateFrom]
                },
            },

        });
        let totalPage = Math.ceil(countProduct / parseInt(size))
        let fullUrl = req.protocol + '://' + req.get('host');

        let data = {
            status: 200,
            msg: "success",
            ProductDB,
            totalPage,
            fullUrl,
            page,
            query,
            token: "Bearer " + req.token,
        };
        // console.log('product : ', data);
        return res.render('admin/product/listProduct', { title: "List Product", data: data });
    } catch (err) {
        let data = {
            status: 500,
            msg: "errr",
            CategoryDB,
            totalPage
        };
        return res.render('admin/product/listProduct', { title: "List Product", data: data });
    }
}
const activeProduct = async (req, res) => {
    const { id } = req.body;
    let statusUpdate;
    let ProductDB = await db.Product.findOne({
        where: { id: id },
        attributes: ['status'],
    });
    if (ProductDB.status == 1) {
        statusUpdate = 0;
    } else {
        statusUpdate = 1;
    }
    console.log('status :', statusUpdate);
    await db.Product.update(
        {
            status: statusUpdate
        },
        { where: { id: id } },
    );
    res.send({ title: "List Product", res: true });
}
const deleteProduct = async (req, res) => {
    const { id } = req.body;
    await db.Product.destroy({
        where: { id: id },
    });
    await db.Discount_product.destroy({
        where: { id_product: id },
    });
    res.send({ title: "List Product", res: true });
}
const updateProduct = async (req, res) => {
    const { id } = req.params;
    console.log("id update :", id);
    let ProductUpdate = await db.sequelize.query(
        "SELECT product.id, product.name,product.description,product.image,product.size,product.color,product.unit,product.price,product.status,category.title,id_category FROM`product` inner join`category` on product.id_category = category.id where product.id = '" + id + "'"
        , { type: db.sequelize.QueryTypes.SELECT }
    );
    let CategoryDB = await db.Category.findAll({
        attributes: ['title', 'id'],
    });
    console.log('ProductUpdate : ', ProductUpdate);
    return res.render('admin/product/updateProduct', { title: "Update Product", data: ProductUpdate, category: CategoryDB });
}
const updateProductPost = async (req, res) => {
    const { id, all_image_product, size, color, description, unit, price, id_category, name } = req.body;
    let nameProductDB = await db.Product.findOne({
        where: { id: id },
        attributes: ['id', 'name']
    });
    console.log("nameProductDB :", nameProductDB.name);
    let ProductDB = await db.Product.findOne({
        where: {
            [Op.and]: [
                { name: name },
                { id_category: id_category }
            ],
            [Op.not]: { name: nameProductDB.name }
        },
        attributes: ['id']
    });

    console.log("ProductDB :", ProductDB);
    if (ProductDB) {
        return res.json({ res: false });
    } else {
        await db.Product.update(
            {
                "id_category": id_category,
                "name": name,
                "price": price,
                "unit": unit,
                "image": all_image_product,
                "size": size,
                "color": color,
                "description": description,
                "updated_date": new Date()
            },
            { where: { id: id } },
        );
        return res.json({ res: true });
    }
}
const createProduct = async (req, res) => {
    console.log('view product', true);
    let CategoryDB = await db.Category.findAll({
        where: {
            status: 1,
        },
        attributes: ['title', 'id'],
    });
    res.render('admin/product/createProduct', { title: "Creat Product", category: CategoryDB });
}
const createProductPost = async (req, res) => {
    const { name, description, all_image_product, category, price, unit, size, color } = req.body;
    let ProductDB = await db.Product.findOne({
        where: {
            [Op.and]: [
                { name: name },
                { id_category: category }
            ]
        },
        attributes: ['id'],
    });
    if (ProductDB) {
        return res.json({ res: false });
    } else {
        try {
            await db.Product.create({
                name: name,
                id_category: category,
                image: all_image_product,
                price: price,
                unit: unit,
                color: color,
                size: size,
                description: description,
                status: 0,
                created_date: new Date()
            });
            return res.json({ res: true });
        }
        catch (err) {
            console.log('err description too maxx', err);
            data = {
                status: 500,
                message: 'err description too maxx',
            }
            return res.json({ res: false, data });
        }

    }
}
module.exports = {
    createProductPost,
    createProduct,
    activeProduct,
    deleteProduct,
    updateProduct,
    updateProductPost,
    listProduct,
}