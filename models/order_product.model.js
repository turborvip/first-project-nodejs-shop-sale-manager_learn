const { DataTypes } = require('sequelize');

function model(sequelize) {
    const attributes = {
        id: { type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, unique: true },
        id_order: { type: DataTypes.INTEGER, allowNull: false, },
        id_product: { type: DataTypes.INTEGER, allowNull: true, },
        quantity: { type: DataTypes.TINYINT, allowNull: true, },
        color: { type: DataTypes.STRING, allowNull: true, defaultValue: '' },
        size: { type: DataTypes.STRING, allowNull: true, defaultValue: '' },
    };

    const options = {
        defaultScope: {
            // exclude hash by default
        },
        scopes: {
            // include hash with this scope
            withHash: { attributes: {}, }
        },
        tableName: 'order_product',
        createdAt: false,
        updatedAt: false
    };

    return sequelize.define('Order_product', attributes, options);
}


module.exports = { model };