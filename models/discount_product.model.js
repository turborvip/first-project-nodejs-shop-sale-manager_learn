const { DataTypes } = require('sequelize');

function model(sequelize) {
    const attributes = {
        id: { type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, unique: true },
        id_product: { type: DataTypes.INTEGER, allowNull: false },
        id_discount: { type: DataTypes.INTEGER, allowNull: false },
        created_date: { type: DataTypes.DATE, allowNull: false },
        del: { type: DataTypes.BOOLEAN, allowNull: true, defaultValue: 0 }
    };

    const options = {
        defaultScope: {
            // exclude hash by default
        },
        scopes: {
            // include hash with this scope
            withHash: { attributes: {}, }
        },
        tableName: 'discount_product',
        createdAt: false,
        updatedAt: false
    };

    return sequelize.define('Discount_product', attributes, options);
}


module.exports = { model };