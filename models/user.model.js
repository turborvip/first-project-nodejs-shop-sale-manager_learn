const { DataTypes } = require('sequelize');


function model(sequelize) {
    const attributes = {
        id: { type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, unique: true },
        user_name: { type: DataTypes.TEXT, allowNull: false },
        password: { type: DataTypes.TEXT, allowNull: false },
        name: { type: DataTypes.STRING, allowNull: false, defaultValue: '' },
        email: { type: DataTypes.STRING, allowNull: false, defaultValue: '' },
        phone: { type: DataTypes.INTEGER, allowNull: true, },
        description: { type: DataTypes.STRING, allowNull: false, defaultValue: '' },
        address: { type: DataTypes.STRING, allowNull: true, defaultValue: '' },
        image: { type: DataTypes.STRING, allowNull: true, },
        role: { type: DataTypes.TINYINT, allowNull: false, },
        status: { type: DataTypes.TINYINT, allowNull: false, defaultValue: '1' },
        created_date: { type: DataTypes.DATE, allowNull: false },
        updated_date: { type: DataTypes.DATE, allowNull: true, },
        del: { type: DataTypes.BOOLEAN, allowNull: true, defaultValue: 0 }
    };

    const options = {
        defaultScope: {
            // exclude hash by default
        },
        scopes: {
            // include hash with this scope
            withHash: { attributes: {}, }
        },
        tableName: 'users',
        createdAt: false,
        updatedAt: false
    };

    return sequelize.define('User', attributes, options);
}


module.exports = { model };