const { DataTypes } = require('sequelize');

function model(sequelize) {
    const attributes = {
        id: { type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, unique: true },
        parent_id: { type: DataTypes.INTEGER, allowNull: false, },
        title: { type: DataTypes.TEXT, allowNull: false },
        description: { type: DataTypes.STRING, allowNull: false, },
        gender: { type: DataTypes.TINYINT, allowNull: false, },
        image: { type: DataTypes.STRING, allowNull: true, },
        status: { type: DataTypes.TINYINT, allowNull: false, defaultValue: '0' },
        created_date: { type: DataTypes.DATE, allowNull: false },
        updated_date: { type: DataTypes.DATE, allowNull: true, },
        del: { type: DataTypes.BOOLEAN, allowNull: true, defaultValue: 0 }
    };

    const options = {
        defaultScope: {
            // exclude hash by default
        },
        scopes: {
            withHash: { attributes: {}, }
        },
        tableName: 'category',
        createdAt: false,
        updatedAt: false
    };

    return sequelize.define('Category', attributes, options);
}


module.exports = { model };