const { DataTypes } = require('sequelize');

function model(sequelize) {
    const attributes = {
        id: { type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, unique: true },
        code: { type: DataTypes.STRING, allowNull: false, defaultValue: '' },
        fullname: { type: DataTypes.STRING, allowNull: true, },
        address: { type: DataTypes.STRING, allowNull: true, defaultValue: '' },
        phone: { type: DataTypes.INTEGER, allowNull: true, },
        method: { type: DataTypes.STRING, allowNull: true, defaultValue: '' },
        total: { type: DataTypes.STRING, allowNull: true, defaultValue: '' },
        description: { type: DataTypes.STRING, allowNull: true, defaultValue: '' },
        status: { type: DataTypes.TINYINT, allowNull: true, defaultValue: '0' },
        handler: { type: DataTypes.STRING, allowNull: true, defaultValue: '' },
        created_date: { type: DataTypes.DATE, allowNull: false },
        updated_date: { type: DataTypes.DATE, allowNull: true, },
        del: { type: DataTypes.BOOLEAN, allowNull: true, defaultValue: 0 }
    };

    const options = {
        defaultScope: {
            // exclude hash by default
        },
        scopes: {
            // include hash with this scope
            withHash: { attributes: {}, }
        },
        tableName: 'order',
        createdAt: false,
        updatedAt: false
    };

    return sequelize.define('Order', attributes, options);
}


module.exports = { model };