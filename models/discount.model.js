const { DataTypes } = require('sequelize');

function model(sequelize) {
    const attributes = {
        id: { type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, unique: true },
        caption: { type: DataTypes.TEXT, allowNull: false },
        description: { type: DataTypes.STRING, allowNull: false, defaultValue: '' },
        value: { type: DataTypes.TINYINT, allowNull: false, defaultValue: '100' },
        author: { type: DataTypes.STRING, allowNull: false, defaultValue: '' },
        status: { type: DataTypes.TINYINT, allowNull: false, defaultValue: '0' },
        created_date: { type: DataTypes.DATE, allowNull: false },
        updated_date: { type: DataTypes.DATE, allowNull: true, },
        del: { type: DataTypes.BOOLEAN, allowNull: true, defaultValue: 0 }
    };

    const options = {
        defaultScope: {
            // exclude hash by default
        },
        scopes: {
            // include hash with this scope
            withHash: { attributes: {}, }
        },
        tableName: 'discount',
        createdAt: false,
        updatedAt: false
    };

    return sequelize.define('Discount', attributes, options);
}


module.exports = { model };