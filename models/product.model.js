const { DataTypes } = require('sequelize');

function model(sequelize) {
    const attributes = {
        id: { type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, unique: true },
        id_category: { type: DataTypes.INTEGER, allowNull: false },
        name: { type: DataTypes.TEXT, allowNull: false },
        price: { type: DataTypes.STRING, allowNull: false, defaultValue: '' },
        unit: { type: DataTypes.STRING, allowNull: false, defaultValue: '' },
        image: { type: DataTypes.STRING, allowNull: true, defaultValue: '' },
        size: { type: DataTypes.STRING, allowNull: true, defaultValue: '' },
        color: { type: DataTypes.STRING, allowNull: true, defaultValue: '' },
        description: { type: DataTypes.STRING, allowNull: false, defaultValue: '' },
        status: { type: DataTypes.TINYINT, allowNull: false, defaultValue: '0' },
        created_date: { type: DataTypes.DATE, allowNull: false },
        updated_date: { type: DataTypes.DATE, allowNull: true, },
        del: { type: DataTypes.BOOLEAN, allowNull: true, defaultValue: 0 }
    };

    const options = {
        defaultScope: {
            // exclude hash by default
        },
        scopes: {
            // include hash with this scope
            withHash: { attributes: {}, }
        },
        tableName: 'product',
        createdAt: false,
        updatedAt: false
    };

    return sequelize.define('Product', attributes, options);
}


module.exports = { model };