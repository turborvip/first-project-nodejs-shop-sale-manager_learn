function totalPrice() {
    let dataCart = JSON.parse(localStorage.getItem('dataCart'));
    let totalPrice = 0;
    for (let i = 0; i < dataCart.length; i++) {
        totalPrice += parseInt(dataCart[i].priceAndUnit.replace(/[^0-9]/g, '')) * parseInt(dataCart[i].quantity);
    }
    return totalPrice;
}
function showCart() {
    let dataCart = JSON.parse(localStorage.getItem('dataCart'));
    let html = '';
    for (let i = 0; i < dataCart.length; i++) {
        html += '<div class="item-added">';
        html += '<div class="row">';
        html += '<div class="col-4">';
        html += '<img src="/image/product/' + dataCart[i].iamge + '" alt = "" class="img-fluid" >';
        html += '</div>';
        html += '<span class="col-8">';
        html += '<p>';
        html += '<span class="name-item-added d-block">' + dataCart[i].name + '</span>';
        html += '<span class="d-block ">';
        html += '<small class="quantity-item-added">' + dataCart[i].quantity + '</small>';
        html += '<small> x </small>';
        html += '<small class="price-item-added">' + dataCart[i].priceAndUnit + '</small>';
        html += '</span>';
        html += '<span class="d-block ">';
        html += '<small class="size-item-added">' + dataCart[i].size + '</small>';
        if (dataCart[i].color) {
            html += '<small> - </small>';
            html += '<small class="color-item-added">';
            html += '<input width="20px" type="color" value="' + dataCart[i].color + '" disabled>';
            html += '</small>';
        }
        html += '</span>';
        html += '</div>';
    };
    $('.cart-body-payment').html(html);
    $('#totalPricePayment').html(totalPrice() + 'USD')
};
showCart();


$('.ship').click(function () {
    $('#address').show();
    $('.ship').addClass("active-method");
    $('.noship').removeClass("active-method")
});
$('.noship').click(function () {
    $('#address').hide();
    $('.noship').addClass("active-method");
    $('.ship').removeClass("active-method")
});
$('#province').change(function () {
    idProvince = $(this).val();
    $.ajax({
        url: '/payment/district',
        type: 'POST',
        data: { idProvince },
        success: function (res) {
            if (res.res) {
                let html = '';
                for (let i = 0; i < res.districtDB.length; i++) {
                    html += '<option value="' + res.districtDB[i].id + '">' + res.districtDB[i].name + '</option>'
                }
                $('#district').append(html);
            }
        },
        error: function (e) {
            console.log(e.message);
        }
    })
})
$('#district').change(function () {
    idDistrict = $(this).val();
    $.ajax({
        url: '/payment/ward',
        type: 'POST',
        data: { idDistrict },
        success: function (res) {
            if (res.res) {
                let html = '';
                for (let i = 0; i < res.wardDB.length; i++) {
                    html += '<option >' + res.wardDB[i].name + '</option>'
                }
                $('#ward').append(html);
            }
        },
        error: function (e) {
            console.log(e.message);
        }
    })
})
$('.payment-on-delivery').click(function () {
    $(this).addClass("active-method");
    $('.payment-by-card').removeClass("active-method")
});
$('.payment-by-card').click(function () {
    $(this).addClass("active-method");
    $('.payment-on-delivery').removeClass("active-method")
});
let recipients = null, method = null;
$('.method-payment-input').click(function () {
    method = $(this).val();
})
$('.noship').click(function () {
    $('.address-detail').val('');
    recipients = 'at store';
});
$('.ship').click(function () {
    recipients = null;
});
$('.btn-place-order').click(async function () {
    let dataCart = JSON.parse(localStorage.getItem('dataCart'))
    let fullname = $('#name').val();
    let phonenumber = $('#phone').val();
    let email = $('#email').val();
    let description = $('#description').val();
    let province = $('#province :selected').text().trim();
    let district = $('#district :selected').text().trim();
    let ward = $('#ward').val();
    let address_detail = $('.address-detail').val();
    let total = $('#totalPricePayment').html();
    if (province && district && ward && address_detail) {
        recipients = address_detail + '-' + ward + '-' + district + '-' + province;
    }
    if (fullname && phonenumber && email && recipients && method && total && dataCart) {
        $.ajax({
            url: '../createOrder',
            type: 'POST',
            data: { fullname, recipients, phonenumber, method, total, description, dataCart },
            dataType: "JSON",
            success: function (res) {
                console.log('res', res);
                $('.toast-success').fadeIn('fast');
                $('.toast-success').fadeOut(3000);
                alert('Turborvip Thanks!...');
            },
            error: function (e) {
                console.log(e.message);
                $('.toast-danger').fadeIn('fast');
                $('.toast-danger').fadeOut(3000);
            }
        });
    } else {
        $('.toast-danger-content').html('You must enter enough information');
        $('.toast-danger').fadeIn('fast');
        $('.toast-danger').fadeOut(3000);
        console.log('information', fullname + phonenumber + email + recipients);
        console.log('method', method);
    }
})

