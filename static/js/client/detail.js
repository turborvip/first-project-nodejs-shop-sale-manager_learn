// tab image
$(".tab-item").on("click", function () {
    $([$(".tab-item")[$(this).index()], $(".tab-content")[$(this).index()]]).addClass("active").siblings().removeClass("active");
})
// end tab image

// zoom
$(document).ready(function () {
    var native_width = 0;
    var native_height = 0;
    $(".magnify").mousemove(function (e) {
        if (!native_width && !native_height) {
            var image_object = new Image();
            image_object.src = $(".small").attr("src");

            native_width = image_object.width;
            native_height = image_object.height;
        }
        else {
            var magnify_offset = $(this).offset();
            var mx = e.pageX - magnify_offset.left;
            var my = e.pageY - magnify_offset.top;

            if (mx < $(this).width() && my < $(this).height() && mx > 0 && my > 0) {
                $(".large").fadeIn(100);
            }
            else {
                $(".large").fadeOut(100);
            }
            if ($(".large").is(":visible")) {
                var rx = Math.round(mx / $(".small").width() * native_width - $(".large").width() / 2) * -1;
                var ry = Math.round(my / $(".small").height() * native_height - $(".large").height() / 2) * -1;
                var bgp = rx + "px " + ry + "px";

                var px = mx - $(".large").width() / 2;
                var py = my - $(".large").height() / 2;
                $(".large").css({ left: px, top: py, backgroundPosition: bgp });
            }
        }
    })
})
// end zoom

// size
$(".size").on("click", function () {
    $([$(".size")[$(this).index()]]).addClass("active-num-size").siblings().removeClass("active-num-size");
})
$(".color").on("click", function () {
    $([$(".color")[$(this).index()]]).addClass("active-num-color").siblings().removeClass("active-num-color");
})
// end size

// quantity
$('.quantity-output').html($('#input-quantity').val());
$('.addition').click(function () {
    let quantity = parseInt($('#input-quantity').val());
    if (quantity == 10) {
        $('.addition').attr("disabled", "")
    } else {
        quantity = quantity + 1;
        $('#input-quantity').val(quantity);
        $('.quantity-output').html(quantity);
    }

})
$('.subtraction').click(function () {
    let quantity = parseInt($('#input-quantity').val());
    if (quantity == 1) {
        $('.subtraction').attr("disabled", "")
    } else {
        quantity = quantity - 1;
        $('#input-quantity').val(quantity);
        $('.quantity-output').html(quantity);
    }

});
