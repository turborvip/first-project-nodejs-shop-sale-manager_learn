function repeatString(string, quantity) {
    for (let i = 0; i < quantity; i++) {
        string = string + string;
    }
    return string;
}
function menuShow(Arr, id, gender) {
    let html = '<div class="row">';
    for (let i = 0; i < Arr.length; i++) {
        if (Arr[i].level == 0) {
            html += '<div class=" col-md-3" id="parent_id' + Arr[i].id + gender + '">';
            html += '<div class="font-weight-bold d-block" >' + Arr[i].title + '</div>';
            html += '</div>';
        } else {
            $('#parent_id' + Arr[i].parent_id + gender + '').ready(function () {
                $('#parent_id' + Arr[i].parent_id + gender + '').append('<div  class="categorychild" id="parent_id' + Arr[i].id + gender + '"><a href="http://localhost:3000/product/?idCategory=' + Arr[i].id + '" >' + repeatString('&nbsp;&nbsp;&nbsp', Arr[i].level) + Arr[i].title + '</a></div>');
            })
        }
    }

    html += '</div>'
    $(id).html(html);
}
$(document).ready(function () {
    $.ajax({
        type: "POST",
        url: 'http://localhost:3000/menu',
        dataType: "JSON",
        success: function (res) {
            let idMale = '#male-category';
            let idFeMale = '#female-category';
            let idChild = '#child-category';
            let categoryMale = res.data.categoryMaleDB;
            let categoryFeMale = res.data.categoryFeMaleDB;
            let categoryChild = res.data.categoryChildDB;
            menuShow(categoryMale, idMale, 'male');
            menuShow(categoryFeMale, idFeMale, 'female');
            menuShow(categoryChild, idChild, 'child');
        }
    });
})

$('.logout').click(function () {
    localStorage.clear();
    location.replace('http://localhost:3000/login');
})
$('.user').click(function () {
    if (localStorage.getItem('user')) {
        location.replace('http://localhost:3000/account');
    } else {
        location.replace('http://localhost:3000/login');
    }

})
