$(document).ready(function () {
    $('.banner-home').owlCarousel({
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 15000,
        items: 1,
        lazyLoad: true,
        autoHeight: true,
    })
    $('.sale-carousel').owlCarousel({
        margin: 5,
        responsive: {
            0: {
                items: 1
            },
            240: {
                items: 2
            },
            567: {
                items: 3
            },
            768: {
                items: 4
            },
            1200: {
                items: 5
            }
        }
    })
    $('.product-carousel').owlCarousel({
        loop: true,
        margin: 5,
        responsive: {
            0: {
                items: 1
            },
            240: {
                items: 2
            },
            567: {
                items: 3
            },
            768: {
                items: 4
            },
            1200: {
                items: 5
            }
        }
    })
    $('.news-carousel').owlCarousel({
        loop: true,
        margin: 5,
        responsive: {
            0: {
                items: 1
            },
            240: {
                items: 1
            },
            576: {
                items: 2
            },
            768: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    })
})

