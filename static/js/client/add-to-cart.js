$('.toast-danger').hide();
$('.toast-success').hide();
$('.cart').click(function () {
    $('.cart-main').focus()
    $('.cart-main').show();
});
$(document).mouseup(function (event) {
    $target = $(event.target);
    if (!$target.closest('.cart-main').length &&
        $('.cart-main').is(":visible")) {
        $('.cart-main').fadeOut(500);
    }
});
let size = '';
let color = '';
$('.size').click(function () {
    size = $(this).data('value');
    console.log('size', size);
})
$('.color').click(function () {
    color = $(this).data('value');
    console.log('color', color);
})

function addToCart(cart, id, iamge, name, priceAndUnit, quantity, size, color) {
    let id_product = id;
    let product = { id_product, iamge, name, priceAndUnit, quantity, size, color };
    cart.push(product);
    cart = JSON.stringify(cart);
    localStorage.setItem('dataCart', cart);
}
function CheckColorSize(sizeHTML, colorHTML) {
    let checkSize = true;
    let checkColor = true;
    if (sizeHTML) {
        if (size) {
            checkSize = true;
        } else {
            checkSize = false
        }
    }
    if (colorHTML) {
        if (color) {
            checkColor = true;
        } else {
            checkColor = false
        }
    }
    if (checkColor && checkSize) {
        return true;
    } else {
        return false;
    }
}
function removeItem(id) {
    let dataCart = JSON.parse(localStorage.getItem('dataCart'));
    let newCart = [];
    for (let i = 0; i < dataCart.length; i++) {
        if (dataCart[i].id_product != id) {
            newCart.push(dataCart[i]);
        }
    }
    console.log('newcart', newCart);
    newCart = JSON.stringify(newCart);
    localStorage.setItem('dataCart', newCart);
}
function totalPrice() {
    let dataCart = JSON.parse(localStorage.getItem('dataCart'));
    let totalPrice = 0;
    for (let i = 0; i < dataCart.length; i++) {
        totalPrice += parseInt(dataCart[i].priceAndUnit.replace(/[^0-9]/g, '')) * parseInt(dataCart[i].quantity);
    }
    return totalPrice;
}
function showCart() {
    let dataCart = JSON.parse(localStorage.getItem('dataCart'));
    let lengtCart = '';
    if (dataCart.length != 0) {
        lengtCart = dataCart.length;
    }
    $('#lengthCart').html(lengtCart);
    console.log('datacart', dataCart);
    let html = '';
    for (let i = 0; i < dataCart.length; i++) {
        html += '<div class="item-added">';
        html += '<div class="row">';
        html += '<div class="col-4">';
        html += '<img src="/image/product/' + dataCart[i].iamge + '" alt = "" class="img-fluid" >';
        html += '</div>';
        html += '<span class="col-8">';
        html += '<p>';
        html += '<span class="name-item-added d-block">' + dataCart[i].name + '</span>';
        html += '<span class="d-block ">';
        html += '<small class="quantity-item-added">' + dataCart[i].quantity + '</small>';
        html += '<small> x </small>';
        html += '<small class="price-item-added">' + dataCart[i].priceAndUnit + '</small>';
        html += '</span>';
        html += '<span class="d-block ">';
        html += '<small class="size-item-added">' + dataCart[i].size + '</small>';
        if (dataCart[i].color) {
            html += '<small> - </small>';
            html += '<small class="color-item-added">';
            html += '<input width="20px" type="color" value="' + dataCart[i].color + '" disabled>';
            html += '</small>';
        }
        html += '</span>';
        html += '</div>';
        html += '<div class="rounded-circle text-center btn-remove-item-added " data-id="' + dataCart[i].id_product + '">x</div>';
        html += '</div>';
    };
    $('.cart-body').html(html);
    $('#totalPrice').html(totalPrice() + 'USD')
    $('.btn-remove-item-added').click(function () {
        $(this).parent().hide();
        let id = $(this).data('id');
        let lengthCart = '';
        if ($('#lengthCart').html() - 1 != 0) {
            lengthCart = $('#lengthCart').html() - 1;
        }
        $('#lengthCart').html(lengthCart);
        removeItem(id);
        console.log('id', id);
        $('#totalPrice').html(totalPrice() + 'USD')
    });
};
$('.btn-add-to-cart').click(function () {
    let cart;
    if (localStorage.getItem('dataCart')) {
        cart = JSON.parse(localStorage.getItem('dataCart'));
    } else {
        cart = [];
    }
    let id = $(this).data('id');
    let sizeHTML = $('#size' + id).html();
    let colorHTML = $('#color' + id).html();
    let name = $('#name' + id).html().trim();
    let image = $('#image' + id).html().trim();
    let priceAndUnit = $('#price' + id).html().trim();
    let quantity = $('#quantity' + id).html();
    console.log('name', name);
    console.log('id', id);
    console.log('priceAndUnit', priceAndUnit)
    console.log('quantity', quantity)
    if (CheckColorSize(sizeHTML, colorHTML)) {
        console.log('next :>')
        $('.toast-success').fadeIn('fast');
        $('.toast-success').fadeOut(700);
        addToCart(cart, id, image, name, priceAndUnit, quantity, size, color);
        showCart();
        size = '';
        color = '';
    } else {
        console.log('You must enter all the options!')
        $('.toast-danger').fadeIn('fast');
        $('.toast-danger').fadeOut(700);
    }
});
showCart();

