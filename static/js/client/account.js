$(document).ready(function () {
    let dataUser = JSON.parse(localStorage.getItem('user'));
    console.log('dataUser', dataUser);
    if (!dataUser) {
        location.replace('http://localhost:3000/login');
    }
    $('#username-acc').val(dataUser[1].username);
    $('#name-acc').val(dataUser[2].name);
    $('#email-acc').val(dataUser[3].email);
    $('#phone-acc').val('0' + dataUser[4].phone);
    $('.btn-update-acc').attr("data-id", dataUser[0].id);
    $('.btn-changepass-acc').attr("data-id", dataUser[0].id);
});
$('.btn-changepass').click(function () {
    $('.changepass').show();
    $('.myaccount').hide();
});
$('.btn-myaccount').click(function () {
    $('.myaccount').show();
    $('.changepass').hide();
});


$('.btn-update-acc').click(function () {
    let id = $(this).data('id');
    let name = $('#name-acc').val();
    let email = $('#email-acc').val();
    let phone = $('#phone-acc').val();
    $.ajax({
        url: '/account/updateInf',
        type: 'POST',
        dataType: 'json',
        data: {
            id, name, email, phone
        },
        success: function (res) {
            if (res.res) {
                localStorage.clear();
                $('.toast-success').fadeIn('fast');
                $('.toast-success').fadeOut(3000);
                setTimeout(location.replace('http://localhost:3000/login'), 100000);
            }
        }
    })
});
$('.btn-changepass-acc').click(function () {
    let id = $(this).data('id');
    let oldpass = $('#oldpass-acc').val();;
    let newpass = $('#newpass-acc').val();
    let repass = $('#repass-acc').val();
    let regexPass = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/;
    let check2 = regexPass.test(repass);
    let check1;
    if (newpass == repass) {
        check1 = true;
    } else {
        check1 = false;
    }
    if (check1 && check2) {
        $.ajax({
            url: '/account/changepass',
            type: 'POST',
            dataType: 'json',
            data: {
                id, repass, oldpass
            },
            success: function (res) {
                if (res.res) {
                    localStorage.clear();
                    setTimeout(location.replace('http://localhost:3000/login'), 100000);
                } else {
                    console.log('false');
                    $('.toast-danger-content').html('Wrong old password');
                    $('.toast-danger').fadeIn('fast');
                    $('.toast-danger').fadeOut(3000);
                }
            }
        })
    } else {
        $('.toast-danger-content').html('There was an error entering the new password');
        $('.toast-danger').fadeIn('fast');
        $('.toast-danger').fadeOut(3000);
    }

});