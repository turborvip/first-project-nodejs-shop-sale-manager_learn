const util = {
     // validate
     validatePass : async (pass) => {
        if (pass && pass.length >= 6) {
            return {
                isValid: true,
                errorMessage: ''
            };
        } else {
            return {
                isValid: false,
                errorMessage: 'Mật khẩu phải lớn hơn 6 ký tự'
            };
        }
    },
    validateConfPass : async (pass, confirmPass) => {
        if (confirmPass === 'undefined') {
            return {
                isValid: false,
                errorMessage: 'Bạn phải xác nhận lại mật khẩu'
            };
        } else if (pass !== confirmPass) {
            return {
                isValid: false,
                errorMessage: 'Xác nhận mật khẩu không đúng!'
            };
        } else if (pass === confirmPass) {
            return {
                isValid: true,
                errorMessage: ''
            };
        }
    }
}