
// const token = "Bearer " + localStorage.getItem('accessToken');
// $.ajaxSetup({
//     headers: {
//         'Authorization': token
//     }
// });
function checkUpload(input) {
    let checkUpload = false;
    if (input.size > 500000) {
        alert(`${input.name} is too big! Max is 500KB`);
    } else if (input.type != "image/jpeg" && input.type != "image/png") {
        alert(`${input[0].name} is not image`);
    } else {
        readURL(this);
        $('#preview').show();
        return checkUpload = true;
    }
    return checkUpload;
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function renameFile(originalFile, newName) {
    return new File([originalFile], newName, {
        type: originalFile.type,
        lastModified: originalFile.lastModified,
    });
}
$(".collapse-category").show();

$("#nav-link-users").click(function () {
    console.log('vào user kkk');
    $("#nav-item-users").addClass("nav-item menu-is-opening menu-open");
})

/////////      Dashboard           /////////// 
$('.link-dashboard').click(function () {
    location.replace('http://localhost:3000/dashboard');
})
/////////      End Dashboard           ///////////

/////////      Order           /////////// 
$(".status-order").click(function () {
    let id = $(this).data().id;
    $.ajax({
        type: "POST",
        url: 'http://localhost:3000/dashboard/listorder/status',
        dataType: "JSON",
        data: { id },
        success: function (res) {
            if (res.res == true) {
                console.log('check', res.res)
                location.reload();
            }
            else {
                console.log('check', false);
                $('.toastrDefaultError').ready(function () {
                    toastr.error('Order is delivering!')
                });

            }
        }
    });
})

$(".delete-order").click(function () {
    let id = $(this).data().id;
    $('.modal-yes').click(function () {
        $.ajax({
            type: "POST",
            url: 'http://localhost:3000/dashboard/listorder/delete',
            dataType: "JSON",
            data: { id },
            success: function (res) {
                if (res.res == true) {
                    location.reload();
                }
                else {
                    console.log('check', false);
                }
            }
        });
    })

})

$("#btn-search-order").click(function () {
    let inputSearch = $('#search-order').val();
    window.location.replace('http://localhost:3000/dashboard/listorder?query=' + inputSearch);
})

$(".detail-order").click(function () {
    let id = $(this).data().id;
    window.location.replace("http://localhost:3000/dashboard/listorder/detail/?id=" + id);

})
/////////      End Order           ///////////

/////////      Users           /////////// 
let image_users = null;

$('#image-users').change(function () {
    let file = $('#image-users')[0].files[0];
    if (checkUpload(file)) {
        readURL(this);
        $('#preview').show();
        console.log('file', file);
    }
})
$('#form-upload-users').submit(function (event) {
    event.preventDefault();
    console.log('submit event');
    let file = $('#image-users')[0].files[0];
    console.log('size', file.size);
    if (checkUpload(file)) {
        let formData = new FormData(document.forms['form-upload-users']);
        console.log(formData)
        $.ajax({
            enctype: 'multipart/form-data',
            data: formData,
            type: "POST",
            url: "/upload-avatar",
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                if (res.res == true) {
                    alert('Upload is success!');
                    image_users = res.data.file;
                }
            },
        });
    }
});
$("#btn-create-users").click(function () {
    let regexUser = /^[A-Za-z0-9_\.]{6,32}$/;  /*- Chứa các ký tự A đến Z, a đến z, 0-9 dấu .  và dấu gạch dưới Độ dài 6 đến 32 ký tự */
    let regexPass = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/; /* Tối thiểu sáu ký tự, ít nhất một chữ cái và một số: */
    let regexEmail = /^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/;
    let regexPhone = /(84|0[3|5|7|8|9])+([0-9]{8})\b/
    let username = $('#usersname').val();
    let name = $('#nameUsers').val();
    let email = $('#emailUsers').val();
    let phone = $('#phoneUsers').val();
    let password = $('#passUsers').val();
    let address = $('#addressUser').val();
    let re_pass = $('#rePassUsers').val();
    let role = $('#roleUsers').val();
    let description = $('#description-users').val();
    let check1 = regexUser.test(username);
    let check2 = regexPass.test(password);
    let check3;
    if (password == re_pass) {
        check3 = true;
    } else {
        check3 = false;
    }
    let check4 = regexEmail.test(email);
    let check5 = regexPhone.test(phone);
    if (image_users == null) {
        alert('You must upload image!')
    }
    if (username && name && email && address && password && re_pass && description && image_users && role && phone) {
        if (check1) {
            if (check4) {
                if (check5) {
                    if (check2) {
                        if (check3) {
                            console.log('check ok : okkkkkkk');
                            $.ajax({
                                type: "POST",
                                url: 'http://localhost:3000/dashboard/createusers',
                                dataType: "JSON",
                                data: { username, name, email, address, description, re_pass, image_users, role, phone },
                                success: function (res) {
                                    if (res.res == true) {
                                        console.log('check', res.res)
                                        $('.alert-danger').attr("style", "display:none");
                                        $('.alert-success').attr("style", "display:static");
                                        $('.toastrDefaultSuccess').ready(function () {
                                            toastr.success('Creat user success.')
                                        });
                                        setTimeout(function () { location.reload(); }, 2000);
                                    }
                                    else {
                                        console.log('check', 'Username da ton tai');
                                        $('.alert-success').attr("style", "display:none");
                                        $('.alert-danger').attr("style", "display:static");
                                        $('.alert-danger').text("Username already exist!");
                                        $('.toastrDefaultError').ready(function () {
                                            toastr.error('Username already exist!')
                                        });
                                    }
                                }
                            });
                        } else {
                            $('.alert-success').attr("style", "display:none");
                            $('.alert-danger').attr("style", "display:static");
                            $('.alert-danger').text("Password doesn't match!");
                            $('.toastrDefaultError').ready(function () {
                                toastr.error("Password doesn't match ")
                            });
                        }
                    } else {
                        $('.alert-success').attr("style", "display:none");
                        $('.alert-danger').attr("style", "display:static");
                        $('.alert-danger').text("Incorrect password format: Minimum six characters, at least one letter and one number");
                        $('.toastrDefaultError').ready(function () {
                            toastr.error('Incorrect password format!')
                        });
                    }
                } else {
                    $('.alert-success').attr("style", "display:none");
                    $('.alert-danger').attr("style", "display:static");
                    $('.alert-danger').text("Incorrect phone ");
                    $('.toastrDefaultError').ready(function () {
                        toastr.error('Incorrect phone!')
                    });
                }
            } else {
                $('.alert-success').attr("style", "display:none");
                $('.alert-danger').attr("style", "display:static");
                $('.alert-danger').text("Incorrect email ");
                $('.toastrDefaultError').ready(function () {
                    toastr.error('Incorrect email!')
                });
            }
        } else {
            $('.alert-success').attr("style", "display:none");
            $('.alert-danger').attr("style", "display:static");
            $('.alert-danger').text("Incorrect username format: Contains the characters A to Z, a to z, 0 - 9 signs.and underscores Length 6 to 32 characters");
            $('.toastrDefaultError').ready(function () {
                toastr.error('Incorrect username format!')
            });
        }
    } else {
        console.log('check : false');
        $(".alert-danger").attr("style", "display:static");
        $('.alert-danger').text("You next fill all information!");
        $('.toastrDefaultError').ready(function () {
            toastr.error('You need to enter all the information!')
        });
    }


})

$(".active-users").click(function () {
    let id = $(this).data().id;
    $.ajax({
        type: "POST",
        url: 'http://localhost:3000/dashboard/listusers/active',
        dataType: "JSON",
        data: { id },
        success: function (res) {
            if (res.res == true) {
                console.log('check', res.res)
                location.reload();
            }
            else {
                console.log('check', false);

            }
        }
    });
})
$(".delete-users").click(function () {
    let id = $(this).data().id;
    $('.modal-yes').click(function () {
        $.ajax({
            type: "POST",
            url: 'http://localhost:3000/dashboard/listusers/delete',
            dataType: "JSON",
            data: { id },
            success: function (res) {
                if (res.res == true) {
                    location.reload();
                }
                else {
                    console.log('check', false);
                }
            }
        });
    })

})

$("#btn-search-users").click(function () {
    let inputSearch = $('#search-users').val();
    window.location.replace('http://localhost:3000/dashboard/listusers?query=' + inputSearch);
})

$('#image-update-users').change(function () {
    let file = $('#image-update-users')[0].files[0];
    if (checkUpload(file)) {
        readURL(this);
        $('#preview').show();
        console.log('file', file);
    }
})
$('#form-upload-users-update').submit(function (event) {
    event.preventDefault();
    console.log('submit event');
    let file = $('#image-update-users')[0].files[0];
    let name_old_file = $('#old-name').val();
    console.log('file', file);
    if (checkUpload(file)) {
        let formData = new FormData(document.forms['form-upload-users-update']);
        formData.append('nameOldFile', name_old_file);
        console.log(formData)
        $.ajax({
            enctype: 'multipart/form-data',
            data: formData,
            type: "POST",
            url: "/upload-avatar",
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                if (res.res == true) {
                    alert('Upload is success!');
                    image_users = res.data.file;
                }
            },
        });
    }
});
$("#upadate-admin").click(function () {
    let id = $(this).data().id;
    let comfirm = $('.confirm-users').is(":checked");
    let name = $('#nameAD').val();
    let email = $('#emailAD').val();
    let address = $('#addressAD').val();
    let phone = $('#phoneAD').val();
    let description = $('#descriptionAD').val();
    let regexEmail = /^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/;
    let regexPhone = /(84|0[3|5|7|8|9])+([0-9]{8})\b/;
    let check = regexEmail.test(email);
    let check2 = regexPhone.test(phone);
    if (check) {
        if (check2) {
            if (comfirm && name && email && address && description && image_users && phone) {
                console.log(id, name, email, address, description, image_users);
                $.ajax({
                    type: "POST",
                    url: 'http://localhost:3000/dashboard/update-user',
                    dataType: "JSON",
                    data: { id, name, email, address, description, image_users, phone },
                    success: function (res) {
                        if (res.res == true) {
                            $('.toastrDefaultSuccess').ready(function () {
                                toastr.success('Update success.')
                            });
                            localStorage.clear();
                            setTimeout(function () { location.replace("http://localhost:3000/logoutshop"); }, 2000);
                        }
                        else {
                            $('.toastrDefaultError').ready(function () {
                                toastr.error('Have a wrong!')
                            });
                        }
                    }
                });
            } else {
                $('.toastrDefaultError').ready(function () {
                    toastr.error("You doesn't comfirm or You need to enter all the information !")
                });
            }
        } else {
            $('.toastrDefaultError').ready(function () {
                toastr.error('Phone is wrong!')
            });
        }
    } else {
        $('.toastrDefaultError').ready(function () {
            toastr.error('Email is wrong!')
        });
    }

})

$("#changepass-admin").click(function () {
    let id = $(this).data().id;
    let regexPass = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/; /* Tối thiểu sáu ký tự, ít nhất một chữ cái và một số: */
    let oldpass = $('#oldpass').val();
    let newpass = $('#newpass').val();
    let repass = $('#repass').val();
    let check1 = regexPass.test(newpass);
    let check2;
    if (newpass == repass) {
        check2 = true;
    } else {
        check2 = false;
    }
    if (oldpass && newpass && repass) {
        if (check1 && check2) {
            console.log(id, oldpass, repass);
            $.ajax({
                type: "POST",
                url: 'http://localhost:3000/dashboard/changepass',
                dataType: "JSON",
                data: { id, oldpass, repass },
                success: function (res) {
                    if (res.res == true) {
                        $('.toastrDefaultSuccess').ready(function () {
                            toastr.success('Update success.')
                        });
                        localStorage.clear();
                        setTimeout(function () { location.replace("http://localhost:3000/logoutshop"); }, 2000);
                    }
                    else {
                        $('.toastrDefaultError').ready(function () {
                            toastr.error('Have a wrong!')
                        });
                    }
                }
            });
        } else {
            $('.toastrDefaultError').ready(function () {
                toastr.error("Password don't match or Password is wrong!")
            });
        }
    } else {
        $('.toastrDefaultError').ready(function () {
            toastr.error("You need to enter all the information !")
        });
    }

})
//////////     End Category      ////////////

/////////      Category           /////////// 

let image_category = null;

$('#image-category').change(function () {
    let file = $('#image-category')[0].files[0];
    if (checkUpload(file)) {
        readURL(this);
        $('#preview').show();
        console.log('file', file);
    }
})
$('#form-upload-category').submit(function (event) {
    event.preventDefault();
    console.log('submit event');
    let file = $('#image-category')[0].files[0];
    console.log('size', file.size);
    if (checkUpload(file)) {
        let formData = new FormData(document.forms['form-upload-category']);
        console.log(formData)
        $.ajax({
            enctype: 'multipart/form-data',
            data: formData,
            type: "POST",
            url: "/upload-category",
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                if (res.res == true) {
                    alert('Upload is success!');
                    image_category = res.data.file;
                }
            },
        });
    }
});
$("#btn-create-category").click(function () {
    let description = $('#description-category').val();
    let gender = $('#gender-category').val();
    let name = $('#namecategory').val();
    let parent_category = $('#parent_category').val();
    console.log("name :", name);
    if (image_category == null) {
        alert('You must upload image!')
    }
    if (name && description && image_category && gender && parent_category) {
        console.log('check : true');
        $.ajax({
            type: "POST",
            url: 'http://localhost:3000/dashboard/createcategory',
            dataType: "JSON",
            data: { parent_category, name, description, image_category, gender },
            success: function (res) {
                if (res.res == true) {
                    console.log('check', res.res)
                    $('.alert-danger').attr("style", "display:none");
                    $('.alert-success').attr("style", "display:static");
                    $('.toastrDefaultSuccess').ready(function () {
                        toastr.success('Creat category success.')
                    });
                    setTimeout(function () { location.reload(); }, 2000);
                }
                else {
                    console.log('check', 'categoryname da ton tai');
                    $('.alert-success').attr("style", "display:none");
                    $('.alert-danger').attr("style", "display:static");
                    $('.alert-danger').text("Category already exist!");
                    $('.toastrDefaultError').ready(function () {
                        toastr.error('Category already exist!')
                    });
                }
            }
        });

    } else {
        console.log('check : false');
        $(".alert-danger").attr("style", "display:static");
        $('.alert-danger').text("You next fill all information!");
        $('.toastrDefaultError').ready(function () {
            toastr.error('You need to enter all the information!')
        });
    }
})

$(".active-category").click(function () {
    let id = $(this).data().id;
    $.ajax({
        type: "POST",
        url: 'http://localhost:3000/dashboard/listcategory/active',
        dataType: "JSON",
        data: { id },
        success: function (res) {
            if (res.res == true) {
                console.log('check', res.res)
                location.reload();
            }
            else {
                console.log('check', false);

            }
        }
    });
})

$(".delete-category").click(function () {
    let id = $(this).data().id;
    $('.modal-yes').click(function () {
        $.ajax({
            type: "POST",
            url: 'http://localhost:3000/dashboard/listcategory/delete',
            dataType: "JSON",
            data: { id },
            success: function (res) {
                if (res.res == true) {
                    location.reload();
                }
                else {
                    console.log('check', false);
                }
            }
        });
    })

})

$('#image-update-category').change(function () {
    let file = $('#image-update-category')[0].files[0];
    if (checkUpload(file)) {
        readURL(this);
        $('#preview').show();
        console.log('file', file);
    }
})
$('#form-upload-category-update').submit(function (event) {
    event.preventDefault();
    console.log('submit event');
    let file = $('#image-update-category')[0].files[0];
    let name_old_file = $('#old-name').val();
    console.log('file', file);
    if (checkUpload(file)) {
        let formData = new FormData(document.forms['form-upload-category-update']);
        formData.append('nameOldFile', name_old_file);
        console.log(formData)
        $.ajax({
            enctype: 'multipart/form-data',
            data: formData,
            type: "POST",
            url: "/upload-category",
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                if (res.res == true) {
                    alert('Upload is success!');
                    image_category = res.data.file;
                }
            },
        });
    }
});
$(".update-category").click(function () {
    let id = $(this).data().id;
    console.log("id :", id);
    window.location.replace('http://localhost:3000/dashboard/listcategory/update/' + id);
})
$("#btn-update-category").click(function () {
    let id = $(this).data().id;
    let description = $('#description-category').val();
    let gender = $('#gender-category').val();
    let name = $('#namecategory').val();
    let parent_category = $('#parent_category').val();
    if (id && parent_category && image_category && description && gender && name) {
        console.log('check : true');
        $.ajax({
            type: "POST",
            url: 'http://localhost:3000/dashboard/listcategory/update',
            dataType: "JSON",
            data: { id, parent_category, image_category, description, gender, name },
            success: function (res) {
                if (res.res == true) {
                    console.log('check', res.res)
                    $('.alert-danger').attr("style", "display:none");
                    $('.alert-success').attr("style", "display:static");
                    $('.alert-success').text("Update category success!");
                    setTimeout(function () { location.reload(); }, 2000);
                }
                else {
                    console.log('check', 'categoryname da ton tai');
                    $('.alert-success').attr("style", "display:none");
                    $('.alert-danger').attr("style", "display:static");
                    $('.alert-danger').text("Category already exist!");
                }
            }
        });

    } else {
        console.log('check : false');
        $(".alert-danger").attr("style", "display:static");
        $('.alert-success').attr("style", "display:none");
    }


})

$("#btn-search-category").click(function () {
    console.log('btn search : ', 'click');
    let inputSearch = $('#search-category').val();
    console.log('inputSearch : ', inputSearch);
    window.location.replace('http://localhost:3000/dashboard/listcategory?query=' + inputSearch);
})
//////////     End Category      ////////////

/////////      Product           /////////// 
let all_image_product = null;
function previewFiles(input, placeToInsertImagePreview) {
    if (input) {
        for (i = 0; i < input.length; i++) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $($.parseHTML('<img>')).attr({ 'src': event.target.result, 'style': 'width:130px' }).appendTo(placeToInsertImagePreview);
            }
            reader.readAsDataURL(input[i]);
        }
    }
}
function checkFiles(file) {
    let check = false;
    for (let i = 0; i < file.length; i++) {
        if (file[i].size > 500000) {
            alert(`${file[i].name} is too big! Max is 500KB`);
            check = false;
            break;
        } else if (file[i].type != "image/jpeg" && file[i].type != "image/png") {
            alert(`${file[i].name} is not image`);
            check = false;
            break;
        } else {
            check = true;
        }
    }
    return check;
}
$('#image-product').change(function () {
    $('#output').html('');
    let file = $('#image-product')[0].files;
    if (checkFiles(file)) {
        previewFiles(file, 'div#output');
        console.log('all_image_product', all_image_product)
    }
})
$('#form-upload-product').submit(function (event) {
    event.preventDefault();
    let file = $('#image-product')[0].files;
    console.log('file', file.length);
    if (checkFiles(file)) {
        if (file.length < 10) {
            let formData = new FormData(document.forms['form-upload-product']);
            console.log(formData)
            $.ajax({
                enctype: 'multipart/form-data',
                data: formData,
                type: "POST",
                url: "/upload-product",
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    if (res.res == true) {
                        alert('Upload is success!');
                        all_image_product = JSON.stringify(res.data.file.splice(0, res.data.file.length - 1));
                    }
                },
            });
        } else {
            alert('Image must < 10');
        }
    }

});
$("#btn-create-product").click(function () {
    let size = [];
    let color = [];
    $('#size :checkbox:checked').each(function () {
        size.push($(this).val());
    });
    $('.color').each(function () {
        color.push($(this).val());
    });
    size = size.length != 0 ? JSON.stringify(size) : null;
    color = color.length != 0 ? JSON.stringify(color) : null;
    let name = $('#nameproduct').val();
    let description = $('#description-product').val();
    let category = parseInt($('#category-product').val());
    let price = parseInt($('#price-product').val());
    let unit = $('#unit-product').val();
    if (all_image_product == null) {
        alert('You must upload image')
    }
    if (name && description && all_image_product && price && unit && category) {
        console.log('check : true');
        $.ajax({
            type: "POST",
            url: 'http://localhost:3000/dashboard/createproduct',
            dataType: "JSON",
            data: { name, description, all_image_product, category, price, unit, size, color },
            success: function (res) {
                if (res.res == true) {
                    console.log('check', res.res)
                    $('.alert-danger').attr("style", "display:none");
                    $('.alert-success').attr("style", "display:static");
                    $('.toastrDefaultSuccess').ready(function () {
                        toastr.success('Creat product success.')
                    });
                    setTimeout(function () { location.reload(); }, 2000);
                }
                else {
                    console.log('check', 'productname da ton tai');
                    $('.alert-success').attr("style", "display:none");
                    $('.alert-danger').attr("style", "display:static");
                    $('.alert-danger').text("Product already exist!");
                    $('.toastrDefaultError').ready(function () {
                        toastr.error('Product already exist!')
                    });
                }
            }
        });

    } else {
        console.log('check : false');
        $(".alert-danger").attr("style", "display:static");
        $('.alert-danger').text("You need to enter all the information!");
        $('.toastrDefaultError').ready(function () {
            toastr.error('You need to enter all the information!')
        });
    }
})


$(".active-product").click(function () {
    let id = $(this).data().id;
    $.ajax({
        type: "POST",
        url: 'http://localhost:3000/dashboard/listproduct/active',
        dataType: "JSON",
        data: { id },
        success: function (res) {
            if (res.res == true) {
                console.log('check', res.res)
                location.reload();
            }
            else {
                console.log('check', false);
            }
        }
    });
})
$(".delete-product").click(function () {
    let id = $(this).data().id;
    $('.modal-yes').click(function () {
        $.ajax({
            type: "POST",
            url: 'http://localhost:3000/dashboard/listproduct/delete',
            dataType: "JSON",
            data: { id },
            success: function (res) {
                if (res.res == true) {
                    location.reload();
                }
                else {
                    console.log('check', false);
                }
            }
        });
    })

})

$('#image-update-product').change(function () {
    $('#output').html('');
    let file = $('#image-update-product')[0].files;
    if (checkFiles(file)) {
        previewFiles(file, 'div#output');
        console.log('all_image_product', all_image_product)
    }
})
$('#form-upload-product-update').submit(function (event) {
    event.preventDefault();
    console.log('submit event');
    let file = $('#image-update-product')[0].files;
    let name_old_files = JSON.parse($('#old-name').val());
    console.log('file', file);
    console.log('name_old_file', name_old_files);
    if (checkFiles(file)) {
        if (file.length < 10) {
            let formData = new FormData(document.forms['form-upload-product-update']);
            formData.append('nameOldFiles', name_old_files);
            console.log('formData', formData)
            $.ajax({
                enctype: 'multipart/form-data',
                data: formData,
                type: "POST",
                url: "/upload-product",
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    if (res.res == true) {
                        alert('Upload is success!');
                        all_image_product = JSON.stringify(res.data.file.splice(0, res.data.file.length - 1));
                        console.log('all_image_product', all_image_product);
                    }
                },
            });
        } else {
            alert('Image must < 10');
        }
    }
});
$(".update-product").click(function () {
    let id = $(this).data().id;
    console.log("id :", id);
    window.location.replace('http://localhost:3000/dashboard/listproduct/update/' + id);
})
$("#btn-update-product").click(function () {
    let size = [];
    let color = [];
    $('#size :checkbox:checked').each(function () {
        size.push($(this).val());
    });
    $('.color').each(function () {
        color.push($(this).val());
    });
    size = size.length != 0 ? JSON.stringify(size) : null;
    color = color.length != 0 ? JSON.stringify(color) : null;
    let id = $(this).data().id;
    let description = $('#description-product').val();
    let name = $('#nameproduct').val();
    let unit = $('#unit-product').val();
    let price = $('#price-product').val();
    let id_category = $('#id-category').val();
    console.log("id :", id);
    console.log("name :", name);
    console.log("all_image_product :", all_image_product);
    console.log("description :", description);
    console.log("id_category :", id_category);
    console.log("unit :", unit);
    console.log("price :", price);
    if (id && all_image_product && name && description && unit && price && id_category) {
        console.log('check : true');
        $.ajax({
            type: "POST",
            url: 'http://localhost:3000/dashboard/listproduct/update',
            dataType: "JSON",
            data: { id, all_image_product, size, color, description, unit, price, id_category, name },
            success: function (res) {
                if (res.res == true) {
                    console.log('check', res.res)
                    $('.alert-danger').attr("style", "display:none");
                    $('.alert-success').attr("style", "display:static");
                    $('.alert-success').text("Update product success!");
                    $('.toastrDefaultSuccess').ready(function () {
                        toastr.success('Update product success!')
                    });
                    setTimeout(function () { location.reload(); }, 2000);
                }
                else {
                    console.log('check', 'product da ton tai');
                    $('.alert-success').attr("style", "display:none");
                    $('.alert-danger').attr("style", "display:static");
                    $('.alert-danger').text("Product already exist!");
                    $('.toastrDefaultError').ready(function () {
                        toastr.error('Product name already exist!')
                    });
                }
            }
        });

    } else {
        console.log('check : false');
        $(".alert-danger").attr("style", "display:static");
        $('.alert-success').attr("style", "display:none");
        $('.toastrDefaultError').ready(function () {
            toastr.error('You need to enter all the information!')
        });
    }


})

$("#btn-search-product").click(function () {
    console.log('btn search : ', 'click');
    let inputSearch = $('#search-product').val();
    console.log('inputSearch : ', inputSearch);
    window.location.replace('http://localhost:3000/dashboard/listproduct?query=' + inputSearch);
})

$("#add-color").click(function () {
    console.log('aaaaa', 'bạn vừa click vào add color')
    $("#all-color").append('<input type="color" class="color m-2" name="favcolor" value = "#ffffff" >');
})
$("#remove-color").click(function () {
    $("#all-color").html('');
})

$('#btn-size-shoes').click(function () {
    let html = '';
    html += '<input type="checkbox" name="size" class="sizeproduct" value="4US" />'
    html += '<label>4&nbsp;&nbsp;</label>'
    html += '<input type = "checkbox" name="size" class="sizeproduct" value = "4.5US" /> '
    html += '<label>4.5&nbsp;&nbsp;</label>'
    html += '<input type = "checkbox" name="size" class="sizeproduct" value = "5US" /> '
    html += '<label>5&nbsp;&nbsp;</label>'
    html += '<input type = "checkbox" name="size" class="sizeproduct" value = "5.5US" /> '
    html += '<label>5.5&nbsp;&nbsp;</label>'
    html += '<input type = "checkbox" name="size" class="sizeproduct" value = "6US" /> '
    html += '<label>6&nbsp;&nbsp;</label>'
    html += '<input type = "checkbox" name="size" class="sizeproduct" value = "6.5US" /> '
    html += '<label>6.5&nbsp;&nbsp;</label>'
    html += '<input type = "checkbox" name="size" class="sizeproduct" value = "7US" /> '
    html += '<label>7&nbsp;&nbsp;</label>'
    html += '<input type = "checkbox" name="size" class="sizeproduct" value = "7.5US" /> '
    html += '<label>7.5&nbsp;&nbsp;</label>'
    html += '<input type = "checkbox" name="size" class="sizeproduct" value = "8US" /> '
    html += '<label>8&nbsp;&nbsp;</label>'
    html += '<input type = "checkbox" name="size" class="sizeproduct" value = "8.5US" /> '
    html += '<label>8.5&nbsp;&nbsp;</label>'
    html += '<input type = "checkbox" name="size" class="sizeproduct" value = "9US" /> '
    html += '<label>9&nbsp;&nbsp;</label>'
    html += '<input type = "checkbox" name="size" class="sizeproduct" value = "9.5US" /> '
    html += '<label>9.5&nbsp;&nbsp;</label>'
    html += '<input type = "checkbox" name="size" class="sizeproduct" value = "10US" /> '
    html += '<label>10&nbsp;&nbsp;</label>'
    html += '<input type = "checkbox" name="size" class="sizeproduct" value = "10.5US" /> '
    html += '<label>10.5&nbsp;&nbsp;</label>'
    html += '<input type = "checkbox" name="size" class="sizeproduct" value = "11US" /> '
    html += '<label>11&nbsp;&nbsp;</label>'
    html += '<input type = "checkbox" name="size" class="sizeproduct" value = "11.5US" /> '
    html += '<label>11.5&nbsp;&nbsp;</label>'
    html += '<input type="checkbox" name="size" class="sizeproduct" value="12US" />'
    html += '<label>12&nbsp;&nbsp;</label>'
    $('#size').html(html);
})
$('#btn-size-clothes').click(function () {
    let html = '';
    html += '<input type="checkbox" name="size" class="sizeproduct" value="XS" />'
    html += '<label>XS&nbsp;&nbsp;</label>'
    html += '<input type="checkbox" name="size" class="sizeproduct" value="S" />'
    html += '<label>S&nbsp;&nbsp;</label>'
    html += '<input type="checkbox" name="size" class="sizeproduct" value="M" />'
    html += '<label>M&nbsp;&nbsp;</label>'
    html += '<input type="checkbox" name="size" class="sizeproduct" value="L" />'
    html += '<label>L&nbsp;&nbsp;</label>'
    html += '<input type="checkbox" name="size" class="sizeproduct" value="XL" />'
    html += '<label>XL&nbsp;&nbsp;</label>'
    html += '<input type="checkbox" name="size" class="sizeproduct" value="XXL" />'
    html += '<label>XXL&nbsp;&nbsp;</label>'
    $('#size').html(html);
})

//////////     End Product      ////////////

/////////      News           /////////// 
let image_news = null;
$('#image-news').change(function () {
    let file = $('#image-news')[0].files[0];
    console.log('file', file);
    if (checkUpload(file)) {
        readURL(this);
        $('#preview').show();
        console.log('file', file);
    }
})
$('#form-upload-news').submit(function (event) {
    event.preventDefault();
    console.log('submit event');
    let file = $('#image-news')[0].files[0];
    console.log('size', file.size);
    if (checkUpload(file)) {
        let formData = new FormData(document.forms['form-upload-news']);
        console.log(formData)
        $.ajax({
            enctype: 'multipart/form-data',
            data: formData,
            type: "POST",
            url: "/upload-news",
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                if (res.res == true) {
                    alert('Upload is success!');
                    image_news = res.data.file;
                }
            },
        });
    }
});
$("#btn-create-news").click(function () {
    let caption = $('#captionnews').val();
    let description = $('#description-news').val();
    let content = CKEDITOR.instances['ckeditor_news'].getData()
    let author = $('#author').val();
    // console.log("caption :", caption);
    // console.log("description :", description);
    // console.log("content :", content);
    // console.log("author :", author);
    // console.log("image_news :", image_news);
    if (image_news == null) {
        alert('You must upload image1')
    }
    if (caption && description && image_news && content && author) {
        console.log('check : true');
        $.ajax({
            type: "POST",
            url: 'http://localhost:3000/dashboard/createnews',
            dataType: "JSON",
            data: { caption, description, content, author, image_news },
            success: function (res) {
                if (res.res == true) {
                    console.log('check', res.res)
                    $('.alert-danger').attr("style", "display:none");
                    $('.alert-success').attr("style", "display:static");
                    $('.toastrDefaultSuccess').ready(function () {
                        toastr.success('Creat news success!')
                    });
                    setTimeout(function () { location.reload(); }, 2000);
                }
                else {
                    console.log('check', 'news da ton tai');
                    $('.alert-success').attr("style", "display:none");
                    $('.alert-danger').attr("style", "display:static");
                    $('.alert-danger').text("News already exist!");
                    $('.toastrDefaultError').ready(function () {
                        toastr.error('News already exist!')
                    });
                }
            }
        });
    } else {
        $(".alert-danger").attr("style", "display:static");
        $('.alert-danger').text("You need to enter all the information!");
        $('.toastrDefaultError').ready(function () {
            toastr.error('You need to enter all the information!')
        });
    }
})

$(".active-news").click(function () {
    let id = $(this).data().id;
    $.ajax({
        type: "POST",
        url: 'http://localhost:3000/dashboard/listnews/active',
        dataType: "JSON",
        data: { id },
        success: function (res) {
            if (res.res == true) {
                console.log('check', res.res)
                location.reload();
            }
            else {
                console.log('check', false);

            }
        }
    });
})

$(".delete-news").click(function () {
    let id = $(this).data().id;
    $('.modal-yes').click(function () {
        $.ajax({
            type: "POST",
            url: 'http://localhost:3000/dashboard/listnews/delete',
            dataType: "JSON",
            data: { id },
            success: function (res) {
                if (res.res == true) {
                    location.reload();
                }
                else {
                    console.log('check', false);
                }
            }
        });
    })

})

$('#image-update-news').change(function () {
    let file = $('#image-update-news')[0].files[0];
    if (checkUpload(file)) {
        readURL(this);
        $('#preview').show();
        console.log('file', file);
    }
})
$('#form-upload-news-update').submit(function (event) {
    event.preventDefault();
    console.log('submit event');
    let file = $('#image-update-news')[0].files[0];
    let name_old_file = $('#old-name').val();
    console.log('file', file);
    if (checkUpload(file)) {
        let formData = new FormData(document.forms['form-upload-news-update']);
        formData.append('nameOldFile', name_old_file);
        console.log(formData)
        $.ajax({
            enctype: 'multipart/form-data',
            data: formData,
            type: "POST",
            url: "/upload-news",
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                if (res.res == true) {
                    alert('Upload is success!');
                    image_news = res.data.file;
                    $('upload-image-category').css('display', 'none');
                }
            },
        });
    }
});
$(".update-news").click(function () {
    let id = $(this).data().id;
    console.log("id :", id);
    window.location.replace('http://localhost:3000/dashboard/listnews/update/' + id);
})
$("#btn-update-news").click(function () {
    let id = $(this).data().id;
    let description = $('#description-news').val();
    let caption = $('#captionnews').val();
    let content = $('#ckeditor_news').val();
    let author = $('#author').val();
    console.log("caption :", caption);
    console.log("description :", description);
    console.log("content :", content);
    console.log("author :", author);
    if (id && caption && description && image_news && content && author) {
        console.log('check : true');
        $.ajax({
            type: "POST",
            url: 'http://localhost:3000/dashboard/listnews/update',
            dataType: "JSON",
            data: { id, caption, description, image_news, content, author },
            success: function (res) {
                if (res.res == true) {
                    console.log('check', res.res)
                    $('.alert-danger').attr("style", "display:none");
                    $('.alert-success').attr("style", "display:static");
                    $('.alert-success').text("Update category success!");
                    $('.toastrDefaultSuccess').ready(function () {
                        toastr.success('Update news success!')
                    });
                    setTimeout(function () { location.reload(); }, 2000);
                }
                else {
                    console.log('check', 'categoryname da ton tai');
                    $('.alert-success').attr("style", "display:none");
                    $('.alert-danger').attr("style", "display:static");
                    $('.alert-danger').text("Category already exist!");
                    $('.toastrDefaultError').ready(function () {
                        toastr.error('News caption already exist!')
                    });
                }
            }
        });

    } else {
        console.log('check : false');
        $(".alert-danger").attr("style", "display:static");
        $('.alert-success').attr("style", "display:none");
        $('.toastrDefaultError').ready(function () {
            toastr.error('You need to enter all the information!')
        });
    }


})

$("#btn-search-news").click(function () {
    console.log('btn search : ', 'click');
    let inputSearch = $('#search-news').val();
    console.log('inputSearch : ', inputSearch);
    window.location.replace('http://localhost:3000/dashboard/listnews?query=' + inputSearch);
})
//////////     End News      ////////////

/////////      Discount           /////////// 
$("#btn-create-discount").click(function () {
    let caption = $('#caption-discount').val();
    let description = $('#description-discount').val();
    let value = parseInt($('#value-discount').val());
    let author = $('#author').val();
    console.log("caption :", caption);
    console.log("description :", description);
    console.log("value :", value);
    console.log("author :", author);

    if (caption && description && author) {
        console.log('check : true');
        $.ajax({
            type: "POST",
            url: 'http://localhost:3000/dashboard/creatediscount',
            dataType: "JSON",
            data: { caption, description, value, author },
            success: function (res) {
                if (res.res == true) {
                    console.log('check', res.res)
                    $('.alert-danger').attr("style", "display:none");
                    $('.alert-success').attr("style", "display:static");
                    $('.alert-success').text("Success");
                    $('.toastrDefaultSuccess').ready(function () {
                        toastr.success('Creat Discount success!')
                    });
                }
                else {
                    console.log('check', 'Discount da ton tai');
                    $('.alert-success').attr("style", "display:none");
                    $('.alert-danger').attr("style", "display:static");
                    $('.alert-danger').text("Discount already exist!");
                    $('.toastrDefaultError').ready(function () {
                        toastr.error('Discount already exist!')
                    });
                }
            }
        });
    } else {
        console.log('check : false');
        $(".alert-danger").attr("style", "display:static");
        $('.alert-danger').text("You next fill all information!");
        $('.toastrDefaultError').ready(function () {
            toastr.error('You need to enter all the information!')
        });
    }
})

$(".active-discount").click(function () {
    let id = $(this).data().id;
    $.ajax({
        type: "POST",
        url: 'http://localhost:3000/dashboard/listdiscount/active',
        dataType: "JSON",
        data: { id },
        success: function (res) {
            if (res.res == true) {
                console.log('check', res.res)
                location.reload();
            }
            else {
                console.log('check', false);

            }
        }
    });
})

$(".delete-discount").click(function () {
    let id = $(this).data().id;
    $('.modal-yes').click(function () {
        $.ajax({
            type: "POST",
            url: 'http://localhost:3000/dashboard/listdiscount/delete',
            dataType: "JSON",
            data: { id },
            success: function (res) {
                if (res.res == true) {
                    location.reload();
                }
                else {
                    console.log('check', false);
                }
            }
        });
    })

})

$(".update-discount").click(function () {
    let id = $(this).data().id;
    console.log("id :", id);
    window.location.replace('http://localhost:3000/dashboard/listdiscount/update/' + id);
})
$("#btn-update-discount").click(function () {
    let id = $(this).data().id;
    let description = $('#description-discount').val();
    let caption = $('#caption-discount').val();
    let value = $('#value-discount').val();
    let author = $('#author').val();
    console.log("caption :", caption);
    console.log("description :", description);
    console.log("value :", value);
    console.log("author :", author);
    if (caption && description && author) {
        console.log('check : true');
        if (value >= 1 && value <= 100) {
            $.ajax({
                type: "POST",
                url: 'http://localhost:3000/dashboard/listdiscount/update',
                dataType: "JSON",
                data: { id, caption, description, value, author },
                success: function (res) {
                    if (res.res == true) {
                        console.log('check', res.res)
                        $('.alert-danger').attr("style", "display:none");
                        $('.alert-success').attr("style", "display:static");
                        $('.toastrDefaultSuccess').ready(function () {
                            toastr.success('Creat Discount success!')
                        });
                    }
                    else {
                        console.log('check', 'Discount da ton tai');
                        $('.alert-success').attr("style", "display:none");
                        $('.alert-danger').attr("style", "display:static");
                        $('.alert-danger').text("Discount already exist!");
                        $('.toastrDefaultError').ready(function () {
                            toastr.error('Discount already exist!')
                        });
                    }
                }
            });
        } else {
            console.log('check', 'Value sai');
            $('.alert-success').attr("style", "display:none");
            $('.alert-danger').attr("style", "display:static");
            $('.alert-danger').text("Value 1-100% !");
            $('.toastrDefaultError').ready(function () {
                toastr.error('Value 1-100% !')
            });
        }
    } else {
        console.log('check : false');
        $(".alert-danger").attr("style", "display:static");
        $('.alert-danger').text("You next fill all information!");
        $('.toastrDefaultError').ready(function () {
            toastr.error('You need to enter all the information!')
        });
    }


})

$("#btn-search-discount").click(function () {
    console.log('btn search : ', 'click');
    let inputSearch = $('#search-discount').val();
    console.log('inputSearch : ', inputSearch);
    window.location.replace('http://localhost:3000/dashboard/listdiscount?query=' + inputSearch);
})

$(".use-discount").click(function () {
    let id = $(this).data().id;
    console.log("id :", id);
    window.location.replace('http://localhost:3000/dashboard/add-discount-product/' + id);
})
$("#btn-search-use-discount").click(function () {
    let id = $('.idDiscount').val();
    let inputSearch = $('#search-use-discount').val();
    console.log('inputSearch : ', inputSearch);
    window.location.replace('http://localhost:3000/dashboard/add-discount-product/' + id + '?query=' + inputSearch);
})

$(".apply-discount").click(function () {
    let idDiscount = parseInt($('.idDiscount').val());
    let idProduct = parseInt($(this).data().id);
    console.log('idProduct = ', idProduct);
    console.log('idDiscount = ', idDiscount);
    if (idDiscount && idProduct) {
        $.ajax({
            type: "POST",
            url: 'http://localhost:3000/dashboard/add-discount-product',
            dataType: "JSON",
            data: { idDiscount, idProduct },
            success: function (res) {
                if (res.res == true) {
                    console.log('check', res.res)
                    $('.alert-danger').attr("style", "display:none");
                    $('.alert-success').attr("style", "display:static");
                    $('.alert-success').text("Success");
                    $('.toastrDefaultSuccess').ready(function () {
                        toastr.success('Apply Discount success!')
                    });
                }
                else {
                    console.log('check', 'Discount da ton tai');
                    $('.alert-success').attr("style", "display:none");
                    $('.alert-danger').attr("style", "display:static");
                    $('.alert-danger').text("Discount already exist!");
                    $('.toastrDefaultError').ready(function () {
                        toastr.error('Discount already exist!')
                    });
                }
            }
        });
    }

});

$("#btn-search-discount-product").click(function () {
    let inputSearch = $('#search-discount-product').val();
    console.log('inputSearch : ', inputSearch);
    window.location.replace('http://localhost:3000/dashboard/list-discount-product/?query=' + inputSearch);
})

$(".delete-discount-product").click(function () {
    let id = $(this).data().id;
    $('.modal-yes').click(function () {
        $.ajax({
            type: "POST",
            url: 'http://localhost:3000/dashboard/list-discount-product/delete',
            dataType: "JSON",
            data: { id },
            success: function (res) {
                if (res.res == true) {
                    location.reload();
                }
                else {
                    console.log('check', false);
                }
            }
        });
    })

})
//////////     End Discount      ////////////

//////////     Logout      ////////////
$('.logout').click(function () {
    localStorage.clear();
    location.replace('http://localhost:3000/logoutshop')
})
//////////     End Logout      ////////////

/////////      CKeditor           /////////// 
CKEDITOR.replace('ckeditor_news')
/////////     End CKeditor        ///////////



